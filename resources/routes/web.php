<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Route::get('/', function () {
  //  return redirect('/login');
//});

Auth::routes();

Route::group(['middleware' => ['auth'], 'prefix' => 'boss/', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::get('category',                        ['as' => 'category.index',           'uses' => 'CategoryController@index']);
    Route::get('category/add',                    ['as' => 'category.add',             'uses' => 'CategoryController@add']);
    Route::post('category/store',                 ['as' => 'category.store',           'uses' => 'CategoryController@store']);
    Route::get('category/{id}/edit',              ['as' => 'category.edit',            'uses' => 'CategoryController@edit']);
    Route::post('category/{id}/update',           ['as' => 'category.update',          'uses' => 'CategoryController@update']);
    Route::get('category/{id}/delete',            ['as' => 'category.delete',          'uses' => 'CategoryController@delete']);

    Route::get('days',                        ['as' => 'days.index',           'uses' => 'DaysController@index']);
    Route::get('days/add',                    ['as' => 'days.add',             'uses' => 'DaysController@add']);
    Route::post('days/store',                 ['as' => 'days.store',           'uses' => 'DaysController@store']);
    Route::get('days/{id}/edit',              ['as' => 'days.edit',            'uses' => 'DaysController@edit']);
    Route::post('days/{id}/update',           ['as' => 'days.update',          'uses' => 'DaysController@update']);
    Route::get('days/{id}/delete',            ['as' => 'days.delete',          'uses' => 'DaysController@delete']);

    Route::get('destination',                        ['as' => 'destination.index',           'uses' => 'DestinationController@index']);
    Route::get('destination/add',                    ['as' => 'destination.add',             'uses' => 'DestinationController@add']);
    Route::post('destination/store',                 ['as' => 'destination.store',           'uses' => 'DestinationController@store']);
    Route::get('destination/{id}/edit',              ['as' => 'destination.edit',            'uses' => 'DestinationController@edit']);
    Route::post('destination/{id}/update',           ['as' => 'destination.update',          'uses' => 'DestinationController@update']);
    Route::get('destination/{id}/delete',            ['as' => 'destination.delete',          'uses' => 'DestinationController@delete']);

    Route::get('setting/home',                        ['as' => 'setting.home.index',           'uses' => 'PositionController@index']);
    Route::get('setting/home/add',                    ['as' => 'setting.home.add',             'uses' => 'PositionController@add']);
    Route::post('setting/home/store',                 ['as' => 'setting.home.store',           'uses' => 'PositionController@store']);
    Route::get('setting/home/{id}/edit',              ['as' => 'setting.home.edit',            'uses' => 'PositionController@edit']);
    Route::post('setting/home/{id}/update',           ['as' => 'setting.home.update',          'uses' => 'PositionController@update']);
    Route::get('setting/home/{id}/delete',            ['as' => 'setting.home.delete',          'uses' => 'PositionController@delete']);

    Route::get('post',                        ['as' => 'post.index',           'uses' => 'PostController@index']);
    Route::get('post/category',                   ['as' => 'post.category',           'uses' => 'PostController@category']);
    Route::get('post/add',                    ['as' => 'post.add',             'uses' => 'PostController@add']);
    Route::post('post/store',                 ['as' => 'post.store',           'uses' => 'PostController@store']);
    Route::get('post/{id}/edit',              ['as' => 'post.edit',            'uses' => 'PostController@edit']);
    Route::post('post/{id}/update',           ['as' => 'post.update',          'uses' => 'PostController@update']);
    Route::get('post/{id}/delete',            ['as' => 'post.delete',          'uses' => 'PostController@delete']);
});




Route::group([  'namespace' => 'Home\\'], function () {
    Route::get('/', ['as' => 'home.home.index', 'uses' => 'IndexController@index']);
    Route::get('category/{slug}',                        ['as' => 'category.index',           'uses' => 'InnerController@index']);
    Route::get('/about',                        ['as' => 'about.index',           'uses' => 'InnerController@about']);
    Route::get('/contact',                        ['as' => 'contact.index',           'uses' => 'InnerController@contact']);
    Route::get('category/{id}/{slug}',                        ['as' => 'category.detail',           'uses' => 'InnerController@about']);
    Route::get('destination/{slug}',                        ['as' => 'destination.index',           'uses' => 'DestinationController@index']);
});