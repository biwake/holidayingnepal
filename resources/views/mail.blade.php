<h3>New Enquiry Request Has Been Received from {{ $name }}</h3>
<p>Package: {{ $pid }}</p>
<p>Name: {{ $name }}</p>
<p>Address: {{ $address }}</p>
<p>Country: {{ $country }}</p>
<p>Mobile No: {{ $mobile }}</p>
<p>Contact No: {{ $contact }}</p>
<p>Email: {{ $email }}</p>
<p>Person package: {{ $personid }}</p>
<p>No Of Person: {{ $person }}</p>
<p>per person price: INR {{ $per }}</p>
<p>Total price: INR {{ $total }}</p>
<P><b>Please contact through email or call as soon as possible.</b></P>