<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ ViewHelper::getData('name', isset($data['row'])?$data['row']:[]) }}" placeholder="Name" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type </label>

    <div class="col-sm-9">
        <select class="col-xs-10 col-sm-5" name="type" id="parent">
            <option value="page" {{ isset($data['row']) && $data['row']->type=='page'?'selected':''  }}>Page</option>
            <option value="about" {{ isset($data['row']) && $data['row']->type=='about'?'selected':''  }}>About</option>
            <option value="contact" {{ isset($data['row']) && $data['row']->type=='contact'?'selected':''  }}>Contact</option>
        </select>

    </div>
</div>

<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Parent </label>

    <div class="col-sm-9">
        <select class="col-xs-10 col-sm-5" name="parent" id="parent">
            <option value="0">None</option>
            @foreach($data['menu'] as $menu)
                <option value="{{ $menu->id }}" {{ isset($data['row']) && $data['row']->cat==$menu->id?'selected':''  }}>{{ $menu->name }}</option>
                @endforeach
        </select>

    </div>
</div>

<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Description </label>

    <div class="col-sm-9">
        <textarea name="description" rows="10" id="description" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('description', isset($data['row'])?$data['row']:[]) }}</textarea>
    </div>
</div>


