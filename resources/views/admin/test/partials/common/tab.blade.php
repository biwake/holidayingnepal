<div class="tabbable tabs-left">
    <ul class="nav nav-tabs" id="">
        <li class="active">
            <a data-toggle="tab" href="#general">
                <i class="pink icon-dashboard bigger-110"></i>
                General
            </a>
        </li>

        <li class="">
            <a data-toggle="tab" href="#images">
                <i class="icon-picture"></i>
                Images
            </a>
        </li>



    </ul>


    @include($view_path.'partials.common.tab-content')

</div>