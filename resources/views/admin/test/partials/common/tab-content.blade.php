<div class="tab-content">
    <div id="general" class="tab-pane active">
        @include($view_path."partials.common.general")
    </div>

    <div id="images" class="tab-pane">
        @include($view_path."partials.common.image")
    </div>


</div>