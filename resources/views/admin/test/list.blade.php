@extends('admin.common.layout.layout')

@section('page_title'){{ trans($trans_path.'content.common.manager') }} | {{ trans('admin/page/general.content.list.list') }} - {{ trans('general.admin_panel') }} @endsection

@section('content')


    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route('admin.users.index') }}">{{ trans('admin/product/general.content.common.manager') }} </a>
                </li>
                <li class="active">{{ trans($trans_path.'content.list.list') }} </li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ trans($trans_path.'content.common.manager') }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'content.list.product_list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif

                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            <label>
                                                <input type="checkbox" class="ace">
                                                <span class="lbl"></span>
                                            </label>
                                        </th>
                                        <th>{{ trans($trans_path.'content.common.main_image') }}</th>
                                        <th>{{ trans($trans_path.'content.common.name') }}</th>
                                        <th>{{ trans($trans_path.'content.common.category') }}</th>
                                        <th>{{ trans($trans_path.'content.common.old_price') }}</th>
                                        <th>{{ trans($trans_path.'content.common.new_price') }}</th>
                                        <th>{{ trans($trans_path.'content.common.quantity') }}</th>

                                        <th class="hidden-480">{{ trans($trans_path.'content.common.status') }}</th>
                                        <th class="hidden-480">Is Featured</th>

                                        <th></th>
                                    </tr>
                                    <tr>
                                        <th class="center">&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th><input type="text" name="product_title" id="product_title" value="{{ request()->has('title')?request()->get('title'):'' }}" class="form-control"></th>
                                        <th><input type="text" name="product_category" id="product_category" value="{{ request()->has('category')?request()->get('category'):'' }}" class="form-control"></th>
                                        <th>
                                            <input type="text" name="product_old_price_start" id="product_old_price_start" value="{{ request()->has('old-price-start')?request()->get('old-price-start'):'' }}" class="form-control">
                                            <input type="text" name="product_old_price_end" id="product_old_price_end" value="{{ request()->has('old-price-end')?request()->get('old-price-end'):'' }}" class="form-control">
                                        </th>
                                        <th>
                                            <input type="text" name="product_new_price_start" id="product_new_price_start" value="{{ request()->has('new-price-start')?request()->get('new-price-start'):'' }}" class="form-control">
                                            <input type="text" name="product_new_price_end" id="product_new_price_end" value="{{ request()->has('new-price-end')?request()->get('new-price-end'):'' }}" class="form-control">
                                        </th>
                                        <th><input type="text" name="product_remaining_quantity" id="product_remaining_quantity" value="{{ request()->has('qty')?request()->get('qty'):'' }}" class="form-control"></th>

                                        <th class="hidden-480">
                                            <select name="product_status" id="product_status" class="form-control">
                                                <option value="all" {{ request()->has('status') && request()->get('status') == 'all'?"selected":"" }}>All</option>
                                                <option value="active" {{ request()->has('status') && request()->get('status') == 'active'?"selected":"" }}>Active</option>
                                                <option value="inactive" {{ request()->has('status') && request()->get('status') == 'inactive'?"selected":"" }}>Inactive</option>
                                            </select>
                                        </th>

                                        <th>
                                            <input type="text" name="product_is_featured" id="product_is_feature" value="{{ request()->has('is-featured')?request()->get('is-featured'):'' }}" class="form-control">
                                        </th>

                                        <th>
                                            <button class="btn btn-xs btn-success" id="product_search_btn">Search</button>
                                        </th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if ($data['rows']->count() > 0)

                                        @foreach($data['rows'] as $row)

                                            <tr>
                                                <td class="center">
                                                    <label>
                                                        <input type="checkbox" class="ace">
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>


                                                <td>
                                                    @if ($row->main_image)
                                                        <img src="{{ asset('images/product/'.$row->main_image ) }}" alt="" width="50px">
                                                    @else
                                                        <p>No Banner.</p>

                                                    @endif
                                                </td>
                                                <td>{{ $row->name }}</td>
                                                <td>{{ $row->category_title }}</td>
                                                <td>{{ $row->old_price }}</td>
                                                <td>{{ $row->new_price }}</td>
                                                <td>{{ $row->quantity }}</td>
                                                <td class="hidden-480">
                                                    @if ($row->status == 1)
                                                        <span class="label label-sm label-success">Active</span>
                                                    @else
                                                        <span class="label label-sm label-info">In-Active</span>
                                                    @endif
                                                </td>

                                                <td class="hidden-480">
                                                    @if ($row->is_featured == 1)
                                                        <span class="label label-sm label-success">Is Featured</span>
                                                    @else
                                                        <span class="label label-sm label-info">Not Is Featurd</span>
                                                    @endif
                                                </td>

                                                <td>
                                                    <div class="btn-group">

                                                        <a class="btn btn-xs btn-info" href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>

                                                        <a class="btn btn-xs btn-danger bootbox-confirm" id="btn-delete" href="{{ route($base_route.'.delete', ['id' => $row->id]) }}">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>

                                                    </div>


                                                </td>
                                            </tr>

                                        @endforeach


                                        <tr>
                                            <td colspan="10">{!! $data['rows']->appends($data['search_queries'])->links() !!}</td>
                                        </tr>

                                    @else

                                        <tr>
                                            <td colspan="10">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootbox.min.js') }}"></script>
    <script>
        $(".bootbox-confirm").on(ace.click_event, function() {
            bootbox.confirm("Are you sure?", function(result) {
                if(result) {
                    //
                }
            });
        });
    </script>


    <script>
        $(document).ready(function () {


            $('#product_is_feature').focusin(function() {
                $(this).parent('th').append('<span id="product_is_featured_hint">Yes | No</span>');
            });

            $('#product_is_feature').focusout(function() {
                $('#product_is_featured_hint').remove();
            });

            // search
            $('#product_search_btn').click(function () {

                var title = $('#product_title').val();
                var category = $('#product_category').val();
                var old_price_start = $('#product_old_price_start').val();
                var old_price_end = $('#product_old_price_end').val();
                var new_price_start = $('#product_new_price_start').val();
                var new_price_end = $('#product_new_price_end').val();
                var remaining_quantity = $('#product_remaining_quantity').val();
                var status = $('#product_status').val();
                var is_featured = $('#product_is_feature').val();
                var url = '{{ route('admin.products.index') }}';


                // url = url + '?title=' + title + '&categroy=' + category;
                var url_int = false;
                if (title != "") {
                    url = url + '?title=' + title;
                    url_int = true;
                }

                if (category != "") {

                    if (url_int) {
                        url = url + '&category=' + category;
                    } else {
                        url = url + '?category=' + category;
                        url_int = true;
                    }

                }

                if (old_price_start != "") {

                    if (url_int) {
                        url = url + '&old-price-start=' + old_price_start;
                    } else {
                        url = url + '?old-price-start=' + old_price_start;
                        url_int = true;
                    }

                }

                if (old_price_end != "") {

                    if (url_int) {
                        url = url + '&old-price-end=' + old_price_end;
                    } else {
                        url = url + '?old-price-end=' + old_price_end;
                        url_int = true;
                    }

                }

                if (new_price_start != "") {

                    if (url_int) {
                        url = url + '&new-price-start=' + new_price_start;
                    } else {
                        url = url + '?new-price-start=' + new_price_start;
                        url_int = true;
                    }

                }

                if (new_price_end != "") {

                    if (url_int) {
                        url = url + '&new-price-end=' + new_price_end;
                    } else {
                        url = url + '?new-price-end=' + new_price_end;
                        url_int = true;
                    }

                }

                if (remaining_quantity != "") {

                    if (url_int) {
                        url = url + '&qty=' + remaining_quantity;
                    } else {
                        url = url + '?qty=' + remaining_quantity;
                        url_int = true;
                    }

                }

                if (status != "") {

                    /*if (status !== 'all') {
                        if (url_int) {
                            url = url + '&status=' + status;
                        } else {
                            url = url + '?status=' + status;
                            url_int = true;
                        }
                    }*/
                    if (url_int) {
                        url = url + '&status=' + status;
                    } else {
                        url = url + '?status=' + status;
                        url_int = true;
                    }

                }

                if (is_featured != "") {

                    if (url_int) {
                        url = url + '&is-featured=' + is_featured;
                    } else {
                        url = url + '?is-featured=' + is_featured;
                    }

                }

                location.href = url;
                return false;
            });


        });
    </script>


@endsection