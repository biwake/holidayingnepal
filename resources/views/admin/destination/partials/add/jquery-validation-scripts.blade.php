<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#listing-form").validate({

            rules: {
                name: {
                    required: true,
                }


            },
            messages: {
                name: {
                    required: "Please input a name.",
                }

            },

        });

    });
</script>
