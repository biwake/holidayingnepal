<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#edit-form").validate({

            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                'category[]': {
                    required: true,
                },
                description: {
                    required: true
                },
                tag: {
                    required: true
                },
                'destination[]': {
                    required: true
                }

            },
            errorPlacement: function(error, element) {
                if (element.is(':checkbox'))
                    error.appendTo($("div#question"));
                else
                    error.insertAfter(element);
            },
            messages: {
                name: {
                    required: "Please input a title.",
                    minlength: "Your title must be at least 5 characters long"
                },
                'category[]': {
                    required: "Please select at least one category",

                },
                description: {
                    required: "Please input a description."
                },
                tag: {
                    required: "Please add a tag."
                },
                'destination[]': {
                    required: "Please add at least one destination."
                },


            },

        });

    });
</script>
