@extends('admin.common.layout.layout')

@section('page_title') {{ $view_title }} - Admin Panel @endsection

@section('content')


    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
            </script>

            @include('admin.category.partials.breadcrumb')

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>
                    {{ $view_title }} Manager
                    <small>
                        <i class="icon-double-angle-right"></i>
                        Info
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">

                                @if (Request::session()->has('message'))
                                    <div class="alert alert-info">
                                        <button type="button" class="close" data-dismiss="alert">
                                            <i class="icon-remove"></i>
                                        </button>
                                        {!! Request::session()->get('message') !!}
                                        <br>
                                    </div>
                                @endif


                                    <form method="post" action="{{ route($base_route.'.infostore', ['id'=>$data['id']]) }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">
                                            SN
                                        </th>
                                        <th>Title</th>
                                        <th>Price</th>
                                        <th>Per Person</th>

                                    </tr>
                                    </thead>


                                    <tbody>

                                    @if ($data['person']->count() > 0)
                                        @php $counter=0; @endphp
                                        @foreach($data['person'] as $row)
                                            @php $counter++ @endphp

                                            <tr>
                                                <td class="center">
                                                    {{ $counter }}
                                                </td>

                                                <td>
                                                    {{ $row->name }}
                                                </td>
                                                <td>
                                                    <input type="hidden" name="id[]" value="{{ $row->id }}">
                                                    <input type="number" name="info[]" class="col-sm-12" style="float:left;" value="{{ MyHelper::getpersonprice($data['id'],$row->id) }}">
                                                </td>
                                                <td>
                                                    <input type="number" name="per[]" class="col-sm-12" style="float:left;" value="{{ MyHelper::getperperson($data['id'],$row->id) }}">
                                                </td>



                                            </tr>
                                        @endforeach

                                    @else

                                        <tr>
                                            <td colspan="7">No data found.</td>
                                        </tr>

                                    @endif

                                    </tbody>



                                </table>
                                    <div class="clearfix form-actions">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button id="form-submit-btn" class="btn btn-info" type="submit">
                                                <i class="icon-ok bigger-110"></i>
                                                Submit
                                            </button>


                                        </div>
                                    </div>
                                    </form>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection

@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootbox.min.js') }}"></script>





@endsection