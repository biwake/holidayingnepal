<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>

<div class="space-4"></div>




<div class="form-group">

    <div class="col-sm-12">
        <textarea name="about" class="col-sm-12" rows="10" id="description" class="col-xs-10 col-sm-5">{{ $data['about']->about }}</textarea>

        <script>
            CKEDITOR.replace( 'description', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        </script>
    </div>
</div>


