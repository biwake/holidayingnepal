<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#listing-form").validate({

            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                parent: {
                    required: true
                },
                position: {
                    required: true,
                    number: true
                }


            },
            messages: {
                name: {
                    required: "Please input a name.",
                    minlength: "Your name must be at least 5 characters long"
                },
                parent: {
                    required: "Please select at least one category"
                },
                position: {
                    required: "Please input a number.",
                    number: "Input must be a number"
                }

            },

        });

    });
</script>
