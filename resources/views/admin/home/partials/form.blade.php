<div class="space-4"></div>




<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Category </label>

    <div class="col-sm-9">
        <select class="col-xs-10 col-sm-5" name="parent" id="parent">
            <option value="">None</option>
            @foreach($data['menu'] as $menu)
                <option value="{{ $menu->id }}"  {{ isset($data['row']) && $data['row']->cid==$menu->id?'selected':''  }}>{{ $menu->name }}</option>
                @endforeach
        </select>

    </div>
</div>

<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ ViewHelper::getData('title', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Position </label>

    <div class="col-sm-9">
        <input type="number" name="position" id="position" value="{{ ViewHelper::getData('pos', isset($data['row'])?$data['row']:[]) }}" placeholder="Position" class="col-xs-10 col-sm-5">
    </div>
</div>


