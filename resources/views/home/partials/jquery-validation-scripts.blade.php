<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>

<script>


    $('#myModal').on('show.bs.modal', function(e) {


        var $modal = $(this),
                esseyId = 'I would like you to contact me for the '+e.relatedTarget.id+ ' package';
        $modal.find('.edit-content').html(esseyId);

        var $mymodal = $(this),
                replicate = e.relatedTarget.name;
        $modal.find('.myreplicate').val(replicate);


    })


</script>

<script>

    $(document).ready(function () {
        $("#loadingimages").hide();
        $("#listing-form").validate({

            rules: {
                name: {
                    required: true,
                    minlength: 5
                },
                address: {
                    required: true,
                    minlength: 5
                },
                country: {
                    required: true
                },
                mobileno: {
                    required: true,
                    minlength: 10
                },
                email: {
                    required: true,
                    minlength: 5,
                    email: true
                },
                person: {
                    required: true
                },
                btnnoofperson: {
                    required: true
                },
                perperson: {
                    required: true
                },
                totalprice: {
                    required: true,
                    number: true
                },
                msg: {
                    required: true
                },

            },
            errorPlacement: function(error, element) {
                $( "#errorTxt" ).empty();
                    error.appendTo($("div#errorTxt"));


            },
            messages: {
                name: {
                    required: "Please input a name.",
                    minlength: "Your name must be at least 5 characters long"
                },
                address: {
                    required: "Please input address.",
                    minlength: "Your address must be at least 5 characters long"
                },
                country: {
                    required: "Please input country."
                },
                mobileno: {
                    required: "Please input mobileno.",
                    minlength: "Mobile NO must be at least 10 characters long"
                },
                email: {
                    required: "Please input email.",
                    minlength: "Email must be at least 5 characters long",
                    email: "Please input valid email"
                },
                person: {
                    required: "Please select person."
                },
                btnnoofperson: {
                    required: "Please input no of person."
                },
                perperson: {
                    required: "Per person amount is required"
                },
                totalprice: {
                    required: "Total amount is required",
                    number: "Please input number between selected person package in No. Of Person"
                },
                msg: {
                    required: "Please write some message for us"
                }


            },

            submitHandler: function(form) {
                $("#loadingimages").show();
                $.ajax({
                    url: form.action,
                    type: form.method,
                    data: $(form).serialize(),
                    success : function(data) {
                        $("#listing-form")[0].reset();
                        $("#btn-price-per-person").val('');
                        $("#btn-total-price").val('');
                        $("#loadingimages").hide();
                        $('#errorTxt').html(data);



                    },
                    error: function(data) {

                        alert('An error occurred while processing the request. Please try again.');
                        $("#loadingimages").hide();

                    }

                });
            }
        });



    });
</script>
