@extends('home.common.layout')
@section('page_title') {{ $data['row']->name }}  @endsection
@section('keyword') {{ $data['row']->tags }} @endsection
@section('description') @endsection
@section('content')
<div id="loadingimage">
<img src = " {{ asset("images/gears.gif") }}" style="margin-left:30%;" />
</div>
    <div class="container" id="grid-row-wrapper">
<div class="grid" id="row-wrapper">

    <div class="col-md-9 badges-Info1" style="padding-bottom: 20px;" >

        <div class="grid" >
        <h4 style="margin-bottom: 0;">{{ $data['row']->name }}</h4>
        </div>


@php
    $img = Image::make('images/post/'.$data['row']->image);

    // crop image
    $img->resize(840, 450)->encode('data-url');
    
@endphp

<img src="{{ $img }}" class="col-md-12 img-responsive" style="height: 450px !important;">


                    <p style="text-align: justify; padding: 12px;">{!! $data['row']->detail  !!} </p>




                <div class="clearfix"> </div>



        @include('home.destination.include.modal')

    </div>

    <div class="col-md-3 badges-Info1" style="margin-top: 20px;">
        <h3 style=" color: #023e84; font-size: 16px;">Book or Customize package now</h3>
        <button type="button" name="{{ $data['row']->slug }}" style="margin-left: 20px;" class="btn btn-info btn-lg col-md-10" id="{{ $data['row']->name }}"  data-toggle="modal" data-target="#myModal">Enquire Or Book Now</button>


        <div class="clearfix"></div>
        <h3 style=" color: #023e84; margin-top: 20px;">Price: IRs {{ $data['row']->price }}</h3>
        <table width="100%" border="1">
            @foreach($data['person'] as $person)
            <tr style="color: #023e84;">
                <td>{{ $person->name }}</td>
                <td><span style="color:red;">{{ ViewHelper::getpriceindetail($data['row']->aid,$person->id) }}</span> per person</td>
            </tr>
            @endforeach
        </table>
        <div class="clearfix"></div>
        <h3 style=" color: #023e84; margin-top: 20px; font-size: 16px; margin-bottom: 10px;">Destination</h3>
        @php $descount=0; @endphp
        @foreach($data['destination'] as $des)
            @php $descount++ @endphp

        <a href="{{ route('destination.index', ['slug'=>$des->slug]) }}" style="color:#bd0c16; ">{{ $des->name }}</a>@if($descount==count($data['destination'])) @else,@endif&nbsp
        @endforeach
        
        <h3 style="background: #023e84; padding: 10px; color: white; font-size: 16px;">{{ count($data['rows']) }} Tour Package</h3>
    <h3 style="font-size: 16px; font-weight: bold; color: #bd0c16; margin-top: 10px; text-align: left;">Filters</h3>
    <h3 style="font-size: 16px; font-weight: bold; color: #bd0c16; margin-top: 20px; text-align: left;">Duration</h3>
        @foreach($data['days'] as $days)
            <input type="radio" value="{{ $days->name }}" name="durationbtn" style="float: left; margin-right: 5px;" {{ isset($data['duration']) && $data['duration']==$days->name?'checked':'' }}><label style="text-align: left; float: left; width: 87%;">{{ $days->name }}</label>
        @endforeach
    <div class="clearfix"></div>


    <h3 style="font-size: 16px; font-weight: bold; color: #bd0c16; margin-top: 20px; text-align: left;">Price</h3>
    @foreach($data['price'] as $price)
        <input type="radio" value="{{ $price->name }}" name="price" style="float: left; margin-right: 5px;" {{ isset($data['money']) && $data['money']==$price->name?'checked':'' }}><label style="text-align: left; float: left; width: 87%;">{{ $price->name }}</label>
    @endforeach
    <div class="clearfix"></div>

        <h3 style="background: #023e84; padding: 10px; color: white; font-size: 16px; margin-top:10px;">Suggested Tours</h3>
        @foreach($data['suggest'] as $suggest)
            <p><a href="{{ ViewHelper::getsearchlink($suggest->cid,$suggest->slug) }}" style="line-height: 25px;">{{ $suggest->name }}</a></p>
        @endforeach

        <h3 style="background: #023e84; padding: 10px; color: white; font-size: 16px; margin-top:10px;">Popular Tours</h3>
        @foreach($data['popular'] as $popular)
            <p><a href="{{ ViewHelper::getsearchlink($popular->cid,$popular->slug) }}" style="line-height: 25px;">{{ $popular->name }}</a></p>
        @endforeach
    </div>
</div>
 @include('home.inner.script')

    </div>
    @endsection

@section('page_script')
    @include('home.partials.jquery-validation-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    @endsection