<div class="footer">
    <div class="container">
        <div class="col-md-3 ftr_navi ftr">
            <h3>Navigation</h3>
            <ul>
                <li><a href="{{ route('home.home.index') }}">Home</a></li>
                <li><a href="{{ route('about.index') }}">About</a></li>
                <li><a href="{{ route('contact.index') }}">Contact</a></li>
            </ul>
        </div>
        <div class="col-md-3 ftr_navi ftr">
            <h3>Things to do</h3>
            <ul>
                @foreach($data['foot'] as $foot)
                <li><a href="{{ route('category.index', ['slug'=>$foot->slug]) }}">{{ $foot->name }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-3 get_in_touch ftr">
            <h3>Get In Touch</h3>
            <p>Thamel</p>
            <p>Kathmandu, Nepal</p>
            <p>+977 01-4-226639</p>
            <p>+977 01-4-220986</p>
            <a href="#">hello@india2nepaltour.com</a>
        </div>
        <div class="col-md-3 ftr-logo">
            <a href="index.html"><h3>India2Nepal Tour</h3></a>
            <p>© Since 2017, India2Nepaltour.com. All rights  Reserved | Managed by <a href="http://holidayingnepal.com" target="_blank">Holidaying Nepal</a> | Co-owned by <a href="http://infinitytours.com.np" target="_blank">Infinity Tours</a> | Powered by &nbsp;<a href="http://onlinemultimediainfo.com" target="_blank">Online Multimedia</a></p>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>