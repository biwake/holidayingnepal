<style type="text/css">
.mylabel{text-align: left; float: left; width: 94%; margin-top:0px;}
</style>

<div class="col-md-3 badges-Info1" style="margin-top: 20px;">
    <h3 style="background: #023e84; padding: 10px; color: white; font-size: 16px;">{{ count($data['rows']) }} Tour Package</h3>
    <h3 style="font-size: 16px; font-weight: bold; color: #bd0c16; margin-top: 10px; text-align: left;">Filters</h3>
    <h3 style="font-size: 16px; font-weight: bold; color: #bd0c16; margin-top: 20px; text-align: left;">Duration</h3>
        @foreach($data['days'] as $days)
            <input type="radio" value="{{ $days->name }}" name="durationbtn" style="float: left; margin-right: 5px;" {{ isset($data['duration']) && $data['duration']==$days->name?'checked':'' }}><label class ="mylabel">{{ $days->name }}</label>
        @endforeach
    <div class="clearfix"></div>


    <h3 style="font-size: 16px; font-weight: bold; color: #bd0c16; margin-top: 20px; text-align: left;">Price</h3>
    @foreach($data['price'] as $price)
        <input type="radio" value="{{ $price->name }}" name="price" style="float: left; margin-right: 5px;" {{ isset($data['money']) && $data['money']==$price->name?'checked':'' }}><label class="mylabel">{{ $price->name }}</label>
    @endforeach
    <div class="clearfix"></div>


    <h3 style="background: #023e84; padding: 10px; color: white; font-size: 16px;">Suggested Tours</h3>
    @foreach($data['suggest'] as $suggest)
    <p><a href="{{ ViewHelper::getsearchlink($suggest->cid,$suggest->slug) }}" style="line-height: 25px;">{{ $suggest->name }}</a></p>
        @endforeach

    <h3 style="background: #023e84; padding: 10px; color: white; font-size: 16px;">Popular Tours</h3>
    @foreach($data['popular'] as $popular)
        <p><a href="{{ ViewHelper::getsearchlink($popular->cid,$popular->slug) }}" style="line-height: 25px;">{{ $popular->name }}</a></p>
    @endforeach


</div>