<div class="header">
    <div class="header-nav" style="border-bottom: 1px solid #2e3192;">
        <div class="container">
            <div class="logo" style="top:5px;">
                <a href="http://india2nepaltour.com" style="font-size:26px;">
                    <!--<span style="top:5px; position:relative;">India2Nepal Tour</span>-->
                   <img src="{{ asset("images/logo.png") }}" style="height:50px; float:left;" alt="India2Nepal Tour"></a>
            </div>
            <div class="navigation">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"> </span>
                            <span class="icon-bar"> </span>
                            <span class="icon-bar"> </span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">



                            @foreach($data['menu'] as $menu)
                            {{ ViewHelper::getmenu($menu->id) }}
                                @endforeach


                        </ul>
                        <div class="clearfix"> </div>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- script-for-menu -->
<script>
    $("span.menu").click(function(){
        $(".top-nav ul").slideToggle("slow" , function(){
        });
    });
</script>