
<!DOCTYPE html>
<html>
@include('home.common.header')

<body>
@include('home.common.menu')


@yield('content')

@include('home.common.footer')

@yield('page_script')



</body>
</html>



