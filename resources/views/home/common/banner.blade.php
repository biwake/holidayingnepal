<style type="text/css">
    .banner {
        min-height: 540px;
        background: url('{{ asset('images/image/'.$data['image']->image) }}') no-repeat -180px 0;
    }
</style>

<div class="banner">
    <script src="{{ asset('front/js/responsiveslides.min.js') }}"></script>
    <script>
        $(function () {
            $("#slider").responsiveSlides({
                auto: true,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                pager: true,
            });
        });
    </script>
    <div class="slider">

        <div class="container">
            <ul class="rslides" id="slider">
                @foreach($data['slider'] as $slider)
                <li>
                    <div class="banner-info">
                        <h2><a href="{{ ViewHelper::getsliderlink($slider->cid,$slider->slug) }}" style="color:#ffffff;"> {{ $slider->name }}</a></h2>
                        <p>{!! str_limit($slider->detail,130) !!}</p>
                    </div>
                </li>
                @endforeach
            </ul>
        </div>
    </div>


</div>
<!--/header-->

<link rel="stylesheet" href="{{ asset('front/css/swipebox.css') }}">
<script src="{{ asset('front/js/jquery.swipebox.min.js') }}"></script>
<script type="text/javascript">
    jQuery(function($) {
        $(".swipebox").swipebox();
    });
</script>