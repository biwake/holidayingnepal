

<ul class="dropdown-menu">
    @foreach(json_decode($data)->value as $thirdmenu)
    <li><a href="{{ route('category.index',['slug'=>$thirdmenu->slug]) }}">{{ $thirdmenu->name }}</a></li>
    @endforeach
</ul>