@extends('home.common.layout')
@section('page_title') {{ $data['slug']->name }} @endsection
@section('keyword') @endsection
@section('description') @endsection
@section('content')
    <div class="container">
    <div id="loadingimage">
<img src = " {{ asset("images/gears.gif") }}" style="margin-left:50%;" />
</div>
        <div class="grid" id="grid-row-wrapper">
            <div id="row-wrapper">
    @include('home.common.left')

    <div class="col-md-9 badges-Info1">
        <div class="grid" >
        <h4 style="margin-bottom: 0;"> {{ $data['slug']->name }}</h4>
        </div>
        @if(count($data['rows'])==0)
            <h3 style="color: #bd0c16;">Sorry, No any travel package found.</h3>
            @else
            @foreach($data['rows'] as $package)

            {{ ViewHelper::getdesgrid($package->pid, $data['slug']->slug) }}
            @endforeach

                {{ $data['rows']->links() }}
            @endif


    </div>
    @include('home.inner.script')
</div>
</div>

    </div>
    @endsection

@section('page_script')
    @include('home.partials.jquery-validation-scripts')

    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    @endsection