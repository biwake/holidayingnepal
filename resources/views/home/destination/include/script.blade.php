<script type="text/javascript">

    $(document).ready(function () {

        $('#loadingimage').hide();


        $('#person-btn-enquire').change(function () {
            var geturl = "{{ URL::route('person.price') }}";
            var package = $('.myreplicate').val();
            var person = $('#person-btn-enquire').val();
            $.ajax({
                url: geturl,
                method: 'POST',
                data: {
                    '_token' : "{{ csrf_token() }}", package : package, person: person,
                },
                success: function (response) {
                    $('#price-wrapper').remove();
                    $('#price-row-wrapper').append(response);
                    $('#btn-no-of-person').focus();
                    $('#loadingimage').hide();
                }
            });

        });


    });

</script>