<div id="price-wrapper">
    <div style="width: 20%; float: left; margin-right: 1%;">
        <label style="width: 100%; text-align: left;">No. Of Person</label>
        <input type="number" name="btnnoofperson" id="btn-no-of-person" style="width: 100%; float: left;">
    </div>

<div style="width: 26%; float: left; margin-right: 1%;">
    <label style="width: 100%; text-align: left;">Price Per Person(IRs)</label>
    <input type="text" style="width: 100%; float: left;" name="perperson" id="btn-price-per-person" value="{{ $data['value']->per }}" readonly>
</div>

<div style="width: 20%; float: left; margin-right: 1%;">
    <label style="width: 100%; text-align: left;">Total Price(IRs)</label>
    <input type="text" style="width: 100%; float: left;" name="totalprice" id="btn-total-price" value="{{ $data['value']->price }}" readonly>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $('#loadingimage').hide();
        $("#btn-no-of-person").bind("change paste keyup", function() {
            var url = "{{ URL::route('person.price.no') }}";
            var no = $('#btn-no-of-person').val();
            var person = $('#person-btn-enquire').val();
            var per = $('#btn-price-per-person').val();
            $.ajax({
                url: url,
                method: 'POST',
                data: {
                    '_token' : "{{ csrf_token() }}", no : no, per: per, person: person,
                },
                success: function (response) {
                    $('#btn-total-price').val(response);
                    $('#loadingimage').hide();
                }
            });

        });




    });
</script>