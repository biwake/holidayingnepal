@extends('home.common.layout')
@section('page_title') Contact Us @endsection
@section('keyword') @endsection
@section('description') @endsection
@section('content')
    <div class="contact">
        <div class="container">
            <h3>Contact</h3>
            <div class="contact-main">
                <div class="contact-top">
                    <div class="col-md-8 contact-top-left">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7064.147531564848!2d85.30789372536051!3d27.71500863115484!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb18fcb77fd4bd%3A0x58099b1deffed8d4!2sThamel%2C+Kathmandu+44600!5e0!3m2!1sen!2snp!4v1488639630985" width="600" height="350" frameborder="0" style="border:0; height: 350px;" allowfullscreen></iframe>
                    </div>
                    <div class="col-md-4 contact-top-right">
                        <div class="contact-textarea">
                            <p style="font-size: 22px;">India2Nepal Tour</p>
                            <p><div class="glyphicon glyphicon-map-marker" style="color: #bd0c16; margin-right: 10px; font-size: 20px;"></div><span style="position: relative; top:-4px;">Thamel, Kathmandu, Nepal</span></p>
                            <p><div class="glyphicon glyphicon-earphone" style="color: #bd0c16; margin-right: 10px; font-size: 20px;"></div><span style="position: relative; top:-4px;">+977 01-4-226639</span></p>
<p><div class="glyphicon glyphicon-earphone" style="color: #bd0c16; margin-right: 10px; font-size: 20px;"></div><span style="position: relative; top:-4px;">+977 01-4-220986</span></p>
                            <p><div class="glyphicon glyphicon-save-file" style="color: #bd0c16; margin-right: 10px; font-size: 20px;"></div><span style="position: relative; top:-4px;">FAX: +977-14-4312456</span></p>
                            <p><div class="glyphicon glyphicon-envelope" style="color: #bd0c16; margin-right: 10px; font-size: 20px;"></div><span style="position: relative; top:-4px;">hello@india2nepaltour.com</span></p>

                            <!--<form>
                                <input type="text" value="First Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'First Name';}">
                                <input type="text" value="Second Name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Second Name';}">
                                <input type="text" value="Email Id" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email Id';}">
                                <textarea value="Message:" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Message';}">Message..</textarea>
                                <input type="submit" value="SUBMIT">
                                <input type="reset" value="CLEAR">
                            </form>-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    @endsection

@section('page_script')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    @endsection