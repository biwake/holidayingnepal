@extends('home.common.layout')
@section('page_title') About Us @endsection
@section('keyword') @endsection
@section('description') @endsection
@section('content')

<style>
p{
color:#000 !important;
}
</style>
    <div class="abouts">

        <!------->
        <div class="who_are">
            <div class="container">


                    <h3>About Us</h3>



                <div class="about-list" style="color:#000;">

{!! $data['about']->about !!}
                </div>
            </div>
        </div>
        <!------->

    </div>
    @endsection

@section('page_script')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    @endsection