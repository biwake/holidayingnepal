
@foreach(json_decode($data)->row as $grid)
<div class="grid_box">
    <figure class="effect-julia">
        <img src="{{ asset('images/post/'.$grid->image) }}" alt="img21"/>


    </figure>
    <h2 style="width: 100%; text-align: left;"><a href="{{ route('category.detail', ['id'=>json_decode($data)->slug->slug,'slug'=>$grid->slug]) }}" title="{{ $grid->name }}" style="font-size: 0.8em;">{{ str_limit($grid->name,59) }}</a></h2>
    <p style="float: left; margin-left:5px;">{{ $grid->days }}</p>
    <div class="clearfix"></div>
    <h2><a href="#"> IRs {{ $grid->price }}</a></h2>
    <div class="clearfix"></div>
    <button type="button" name="{{ $grid->slug }}" style="width:97%;" class="btn btn-info btn-lg" id="{{ $grid->name }}" data-content="check" data-toggle="modal" data-target="#myModal">Enquire Now</button>
</div>
    @endforeach
<div class="clearfix"></div>
<a href="{{ route('category.index', ['slug'=>json_decode($data)->slug->slug]) }}" style="background: #2e3192; position:relative; top:15px; margin-top:10px; padding: 10px 30px; font-size: 18px; color: #ffffff;">See More {{ json_decode($data)->title }}</a>
@include('home.destination.include.modal')

