<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'staff',
            'email' => 'staff@gmail.com',
            'password' => bcrypt('staff@12'),
            'name' => 'Staff',
            'status' => true,
            'code' =>1,
            'type'=>'staff',
        ]);
    }
}
