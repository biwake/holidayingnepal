<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/',                        ['as' => 'home.home.index',           'uses' => 'Home\\IndexController@index']);
Route::get('/home', function () {
    return redirect()->route('admin.category.index');
});
Route::get('/aboutus',                        ['as' => 'about.index',           'uses' => 'Home\\InnerController@about']);
Route::get('/contactus',                        ['as' => 'contact.index',           'uses' => 'Home\\InnerController@contact']);
Route::get('/{slug}',                        ['as' => 'category.index',           'uses' => 'Home\\InnerController@index']);
Route::post('/category/ajax/search',                        ['as' => 'category.search',           'uses' => 'Home\\InnerController@search']);
Route::get('/{id}/{slug}',                        ['as' => 'category.detail',           'uses' => 'Home\\InnerController@detail']);
Route::get('/destination/city/{slug}',                        ['as' => 'destination.index',           'uses' => 'Home\\DestinationController@index']);
Route::post('/person/price/get',                        ['as' => 'person.price',           'uses' => 'Home\\DestinationController@personprice']);
Route::post('/person/price/get/no',                        ['as' => 'person.price.no',           'uses' => 'Home\\DestinationController@personpriceno']);
Route::post('/enquiry/form/record/mail',                        ['as' => 'enquiry.mail.index',           'uses' => 'MailController@basic_email']);


Route::group(['middleware' => ['auth'], 'prefix' => 'boss/', 'as' => 'admin.', 'namespace' => 'Admin\\'], function () {
    Route::get('dashboard', ['as' => 'dashboard', 'uses' => 'DashboardController@index']);

    Route::get('category/list',                        ['as' => 'category.index',           'uses' => 'CategoryController@index']);
    Route::get('category/add',                    ['as' => 'category.add',             'uses' => 'CategoryController@add']);
    Route::post('category/store',                 ['as' => 'category.store',           'uses' => 'CategoryController@store']);
    Route::get('category/{id}/edit',              ['as' => 'category.edit',            'uses' => 'CategoryController@edit']);
    Route::post('category/{id}/update',           ['as' => 'category.update',          'uses' => 'CategoryController@update']);
    Route::get('category/{id}/delete',            ['as' => 'category.delete',          'uses' => 'CategoryController@delete']);

    Route::get('image/list',                        ['as' => 'image.index',           'uses' => 'ImageController@index']);
    Route::get('image/add',                    ['as' => 'image.add',             'uses' => 'ImageController@add']);
    Route::post('image/store',                 ['as' => 'image.store',           'uses' => 'ImageController@store']);

    Route::get('user/list',                        ['as' => 'user.index',           'uses' => 'UserController@index']);
    Route::get('user/add',                    ['as' => 'user.add',             'uses' => 'UserController@add']);
    Route::post('user/store',                 ['as' => 'user.store',           'uses' => 'UserController@store']);
    Route::get('user/{id}/edit',              ['as' => 'user.edit',            'uses' => 'UserController@edit']);
    Route::post('user/{id}/update',           ['as' => 'user.update',          'uses' => 'UserController@update']);
    Route::get('user/{id}/delete',            ['as' => 'user.delete',          'uses' => 'UserController@delete']);

    Route::get('budget/list',                        ['as' => 'budget.index',           'uses' => 'BudgetController@index']);
    Route::get('budget/add',                    ['as' => 'budget.add',             'uses' => 'BudgetController@add']);
    Route::post('budget/store',                 ['as' => 'budget.store',           'uses' => 'BudgetController@store']);
    Route::get('budget/{id}/edit',              ['as' => 'budget.edit',            'uses' => 'BudgetController@edit']);
    Route::post('budget/{id}/update',           ['as' => 'budget.update',          'uses' => 'BudgetController@update']);
    Route::get('budget/{id}/delete',            ['as' => 'budget.delete',          'uses' => 'BudgetController@delete']);

    Route::get('person/list',                        ['as' => 'person.index',           'uses' => 'PersonController@index']);
    Route::get('person/add',                    ['as' => 'person.add',             'uses' => 'PersonController@add']);
    Route::post('person/store',                 ['as' => 'person.store',           'uses' => 'PersonController@store']);
    Route::get('person/{id}/edit',              ['as' => 'person.edit',            'uses' => 'PersonController@edit']);
    Route::post('person/{id}/update',           ['as' => 'person.update',          'uses' => 'PersonController@update']);
    Route::get('person/{id}/delete',            ['as' => 'person.delete',          'uses' => 'PersonController@delete']);

    Route::get('price/list',                        ['as' => 'price.index',           'uses' => 'PriceController@index']);
    Route::get('price/add',                    ['as' => 'price.add',             'uses' => 'PriceController@add']);
    Route::post('price/store',                 ['as' => 'price.store',           'uses' => 'PriceController@store']);
    Route::get('price/{id}/edit',              ['as' => 'price.edit',            'uses' => 'PriceController@edit']);
    Route::post('price/{id}/update',           ['as' => 'price.update',          'uses' => 'PriceController@update']);
    Route::get('price/{id}/delete',            ['as' => 'price.delete',          'uses' => 'PriceController@delete']);

    Route::get('post/list',                        ['as' => 'post.index',           'uses' => 'PostController@index']);
    Route::get('post/category',                   ['as' => 'post.category',           'uses' => 'PostController@category']);
    Route::get('post/search',                   ['as' => 'post.search',           'uses' => 'PostController@search']);
    Route::get('post/add',                    ['as' => 'post.add',             'uses' => 'PostController@add']);
    Route::post('post/store',                 ['as' => 'post.store',           'uses' => 'PostController@store']);
    Route::get('post/{id}/edit',              ['as' => 'post.edit',            'uses' => 'PostController@edit']);
    Route::post('post/{id}/update',           ['as' => 'post.update',          'uses' => 'PostController@update']);
    Route::get('post/{id}/delete',            ['as' => 'post.delete',          'uses' => 'PostController@delete']);
    Route::get('post/{id}/info',            ['as' => 'post.info',          'uses' => 'PostController@info']);
    Route::post('post/{id}/infostore',            ['as' => 'post.infostore',          'uses' => 'PostController@infostore']);
    Route::get('post/{id}/suggest/{slug}',            ['as' => 'post.suggest',          'uses' => 'PostController@suggest']);

    Route::get('days/list',                        ['as' => 'days.index',           'uses' => 'DaysController@index']);
    Route::get('days/add',                    ['as' => 'days.add',             'uses' => 'DaysController@add']);
    Route::post('days/store',                 ['as' => 'days.store',           'uses' => 'DaysController@store']);
    Route::get('days/{id}/edit',              ['as' => 'days.edit',            'uses' => 'DaysController@edit']);
    Route::post('days/{id}/update',           ['as' => 'days.update',          'uses' => 'DaysController@update']);
    Route::get('days/{id}/delete',            ['as' => 'days.delete',          'uses' => 'DaysController@delete']);

    Route::get('destination/list',                        ['as' => 'destination.index',           'uses' => 'DestinationController@index']);
    Route::get('destination/add',                    ['as' => 'destination.add',             'uses' => 'DestinationController@add']);
    Route::post('destination/store',                 ['as' => 'destination.store',           'uses' => 'DestinationController@store']);
    Route::get('destination/{id}/edit',              ['as' => 'destination.edit',            'uses' => 'DestinationController@edit']);
    Route::post('destination/{id}/update',           ['as' => 'destination.update',          'uses' => 'DestinationController@update']);
    Route::get('destination/{id}/delete',            ['as' => 'destination.delete',          'uses' => 'DestinationController@delete']);

    Route::get('setting/home',                        ['as' => 'setting.home.index',           'uses' => 'PositionController@index']);
    Route::get('setting/home/add',                    ['as' => 'setting.home.add',             'uses' => 'PositionController@add']);
    Route::post('setting/home/store',                 ['as' => 'setting.home.store',           'uses' => 'PositionController@store']);
    Route::get('setting/home/{id}/edit',              ['as' => 'setting.home.edit',            'uses' => 'PositionController@edit']);
    Route::post('setting/home/{id}/update',           ['as' => 'setting.home.update',          'uses' => 'PositionController@update']);
    Route::get('setting/home/{id}/delete',            ['as' => 'setting.home.delete',          'uses' => 'PositionController@delete']);
});

