<?php
namespace App\HelperClasses;

use App\Models\Articlecid;
use App\Models\Despost;
use App\Models\Pperson;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\Post;

class MyHelper
{
    public function test()
    {
        echo 'Hellow World!!!';
    }

    public function categoryname($id)
    {

        if($id==0){
            return "None";
        }else {
            $name = Category::select('name')->where('id', $id)->first();
            if (isset($name->name)){

                return rtrim($name->name);
            }else {
                return "Not Available";
            }
        }
    }

    public function articalcategoryname($id)
    {
        $name = Articlecid::select('article_cat.cid as cid','category.name as name')
            ->leftjoin('category', 'category.id', '=', 'article_cat.cid' )
            ->where('aid',$id)
        ->get();

        foreach ($name as $okay){
            $categories[]=$okay->name;
        }
        return implode(", ",$categories);
    }

    public function getcategoryid($aid,$cid)
    {
        if (Articlecid::select('cid')->where('aid',$aid)->where('cid',$cid)->count()==0){
            return '';
        }else{
            return 'checked';
        }

    }

    public function getdestinationid($aid,$did)
    {
        if (Despost::select('id')->where('pid',$aid)->where('did',$did)->count()==0){
            return '';
        }else{
            return 'checked';
        }

    }

    public function getpersonprice($aid,$pid)
    {
        $value = Pperson::select('price')->where('aid',$aid)->where('pid',$pid)->first();
        if (count($value)==0){
            return '';
        }else{
            return rtrim($value->price);
        }
    }

    public function getperperson($aid,$pid)
    {
        $value = Pperson::select('per')->where('aid',$aid)->where('pid',$pid)->first();
        if (count($value)==0){
            return '';
        }else{
            return rtrim($value->per);
        }
    }
}