<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Home;


use App\Models\Articlecid;
use App\Models\Budget;
use App\Models\Category;
use App\Models\Days;
use App\Models\Despost;
use App\Models\Destination;
use App\Models\Person;
use App\Models\Position;
use App\Models\Post;
use App\Models\Pperson;
use App\Models\Price;
use DB, File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class InnerController extends HomeBaseController
{
    protected $base_route = 'home.inner';
    protected $view_path = 'home.inner';

    public function index(Request $request, $slug)
    {

        $data = [];
        //$data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->groupBy('aid')->paginate(15);
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        $data['slug'] = Category::select('id','name','description','slug')->where('slug',$slug)->first();
        $data['rows'] = Articlecid::select('article.name as name','article.tags as tags','article.aid as aid','article.status as status','article.created_at','article.image as image','days.name as days','article.price as price','article.slug as slug')
            ->leftjoin('article', 'article.aid', '=', 'article_cat.aid')
            ->leftjoin('days', 'days.id', '=', 'article.days')
            ->where('article_cat.cid',$data['slug']->id)
->orderBy('article.id', 'desc')
            ->paginate(10);
        $data['budget'] = Budget::select('id','name')->get();
        $data['price'] = Price::select('id','name')->get();
        $data['days'] = Days::select('id','name')->get();
        $data['city'] = Destination::select('id','name')->get();
        $data['person'] = Person::select('id','name')->get();
        $data['suggest'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('suggested','y')
            ->groupby('article_cat.aid')
            ->limit(7)
            ->get();
        $data['popular'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('popular','!=',0)->OrderBy('popular','desc')->groupBy('article_cat.aid')->limit(7)->get();
        $data['foot'] = Category::select('name','slug')->where('cat',0)->where('type','!=','about')->where('type','!=','contact')->limit(5)->get();
        $data['people'] = Person::select('name')->get();
        $data['type'] = 'home';
        return view(parent::loadDefaultVars($this->view_path.'.index'), compact('data'));
    }

    public function about(Request $request)
    {
        $data = [];
        //$data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->groupBy('aid')->paginate(15);
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        $data['cid'] = Category::select('id')->where('type','about')->first();
        $data['about']= Post::select('name','detail')->where('cid',$data['cid']->id)->first();
        $data['foot'] = Category::select('name','slug')->where('cat',0)->where('type','!=','about')->where('type','!=','contact')->limit(5)->get();
        return view(parent::loadDefaultVars('home.about.index'), compact('data'));
    }

    public function contact(Request $request)
    {
        $data = [];
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        $data['foot'] = Category::select('name','slug')->where('cat',0)->where('type','!=','about')->where('type','!=','contact')->limit(5)->get();
        return view(parent::loadDefaultVars('home.contact.index'), compact('data'));
    }

    public function detail(Request $request, $id, $slug)
    {
        $data = [];
        //$data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->groupBy('aid')->paginate(15);
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        //$data['slug'] = Category::select('id','name','description')->where('slug',$slug)->first()
        $data['row']=Post::select('*')->where('slug',$slug)->first();
        $data['destination'] = Despost::select('destination.name as name','destination.slug as slug')
            ->leftjoin('destination', 'destination.id', '=', 'despost.did')
            ->where('pid',$data['row']->aid)->get();
        $pop = $data['row']->popular;
        if ($pop==0){
            $popular =1;
        }else{
            $popular=$pop+1;
        }
        $popu = Post::where('id',$data['row']->id)->update([
           'popular' => $popular
        ]);
        $data['suggest'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('suggested','y')->limit(7)
            ->groupby('article_cat.aid')
            ->get();
        $data['popular'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('popular','!=',0)->OrderBy('popular','desc')->groupBy('article_cat.aid')->limit(7)->get();
        $data['person'] = Person::select('id','name')->get();
        $data['foot'] = Category::select('name','slug')->where('cat',0)->where('type','!=','about')->where('type','!=','contact')->limit(5)->get();
        $data['people'] = Person::select('name')->get();
        $data['type'] = 'home';
        return view(parent::loadDefaultVars('home.detail.index'), compact('data'));
    }

    public function search(Request $request)
    {
        $data = [];
        //$data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->groupBy('aid')->paginate(15);
        $data['duration'] = $request->get('duration');
        $data['money'] = $request->get('price');
        $data['about'] = Category::select('id')->where('type','about')->first();
        if(isset($data['about']->id)){
            $about = $data['about']->id;
        }else{
            $about='';
        }
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        $data['slug'] = Category::select('id','name','description','slug')->where('slug','kathmandu-tours')->first();

            $data['rows'] = Articlecid::select('article.name as name', 'article_cat.cid as cid','article.tags as tags','article.aid as aid','article.status as status','article.created_at','article.image as image','days.name as days','article.price as price','article.slug as slug')
                ->leftjoin('article', 'article.aid', '=', 'article_cat.aid')
                ->leftjoin('days', 'days.id', '=', 'article.days')
                ->where(function ($query) use ($request) {


                    if ($request->has('duration')) {
                        $data['duration'] = Days::select('id')->where('name', $request->get('duration'))->first();
                        $query->where('article.days', $data['duration']->id);
                        $this->search_queries['duration'] = $request->get('duration');
                    }




                    if ($request->has('price')) {
                        $between = Price::select('start', 'end')->where('name', $request->get('price'))->first();
                        $query->whereBetween('price', [$between->start, $between->end]);
                        $this->search_queries['price'] = $request->get('price');
                    }

                })
                ->where('article_cat.cid', '!=', $about)
                ->groupBy('article_cat.aid')
                ->get();


        $data['price'] = Price::select('id','name')->get();
        $data['days'] = Days::select('id','name')->get();
        $data['suggest'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('suggested','y')->limit(7)
            ->groupby('article_cat.aid')
            ->get();
        $data['popular'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('popular','!=',0)->OrderBy('popular','desc')->groupBy('article_cat.aid')->limit(7)->get();
        $data['search_queries'] = $this->search_queries;
        $data['people'] = Person::select('name')->get();
        $data['type'] = 'home';

        return view(parent::loadDefaultVars($this->view_path.'.search'), compact('data'));
    }





}