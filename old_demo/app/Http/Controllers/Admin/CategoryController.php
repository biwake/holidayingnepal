<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Category\CategoryAddValidation;
use App\Http\Requests\Category\CategoryEditValidation;
use App\Models\Category;
use DB, File;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoryController extends AdminBaseController
{
    protected $base_route = 'admin.category';
    protected $view_path = 'admin.category';
    protected $view_title = 'Category';
    protected $folder_name = 'category';
    protected $folder_path;

    public function __construct()
    {
        // parent::__construct();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        if (Auth::user()->type=="admin") {
            $data = [];
            $data['rows'] = Category::select('id', 'name', 'slug', 'cat', 'status')->orderBy('id', 'desc')->get();

            return view(parent::loadDefaultVars($this->view_path . '.list'), compact('data'));
        }else{
            $request->session()->flash('message', 'You are not allowed to access here.');
            return redirect()->route('admin.post.index');
        }
    }

    public function add(Request $request)
    {
        if (Auth::user()->type=="admin") {
        $data = [];
        $data['menu'] = Category::select('id', 'name')->orderBy('name', 'asc')->get();
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
        }else{
            $request->session()->flash('message', 'You are not allowed to access here.');
            return redirect()->route('admin.post.index');
        }
    }

    public function store(CategoryAddValidation $request)
    {
            $sendslug = str_slug($request->get('name'));
            if (Category::select('id')->where('slug',$sendslug)->count()==0){
                $slug =$sendslug;
            }else {
                $slug = $sendslug . '-' . rand(11, 999999);
            }

       Category::create([
           'name'   => $request->get('name'),
           'slug'   => $slug,
           'cat' => $request->get('parent'),
           'description'   => $request->get('description'),
           'status' => 'y',
           'type' => $request->get('type')
        ]);

        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        if (Auth::user()->type=="admin") {
        // get user data for $id
        $data = [];
        if (!$data['row'] = Category::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['menu'] = Category::select('id', 'name')->orderBy('name', 'asc')->get();
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }else{
$request->session()->flash('message', 'You are not allowed to access here.');
return redirect()->route('admin.post.index');
}
    }

    public function update(CategoryEditValidation $request, $id)
    {
        if (!$page = Category::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $page->update([
            'name'   => $request->get('name'),
            'cat' => $request->get('parent'),
            'description'   => $request->get('description'),
            'type' => $request->get('type')
        ]);

        // update page Set menu_id = 1, title = 'title'.....
        // where id = 4;


        $request->session()->flash('message', $this->view_title.' updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (Auth::user()->type=="admin") {
        if (!$page = Category::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $page->delete();
        $request->session()->flash('message', $this->view_title.' deleted successfully.');
        return redirect()->route($this->base_route.'.index');
        }else{
            $request->session()->flash('message', 'You are not allowed to access here.');
            return redirect()->route('admin.post.index');
        }
    }



}