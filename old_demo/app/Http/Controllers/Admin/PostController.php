<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Category\CategoryAddValidation;
use App\Http\Requests\category\CategoryEditValidation;
use App\Http\Requests\Post\PostAddValidation;
use App\Http\Requests\Post\PostEditValidation;
use App\Models\Articlecid;
use App\Models\Category;
use App\Models\Days;
use App\Models\Despost;
use App\Models\Destination;
use App\Models\Person;
use App\Models\Post;
use App\Models\Pperson;
use App\User;
use DB, File;
use Illuminate\Http\Request;

class PostController extends AdminBaseController
{
    protected $base_route = 'admin.post';
    protected $view_path = 'admin.post';
    protected $view_title = 'Post';
    protected $folder_name = 'post';
    protected $folder_path;

    public function __construct()
    {
        // parent::__construct();
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at','suggested')->orderBy('id','desc')->paginate(30);
        $data['menu'] = Category::select('slug','name')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function category(Request $request)
    {
        $data = [];
        $data['id'] = Category::select('id')->where('slug',$request->get('title'))->first();
        $data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->where('cid',$data['id']->id)->groupBy('aid')->paginate(100);
        $data['menu'] = Category::select('slug','name')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function search(Request $request)
    {
        $data = [];
        $data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->where('name', 'LIKE', "%$request->title%")->paginate(100);
        $data['title'] = $request->get('title');
        $data['menu'] = Category::select('slug','name')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];
        $data['menu'] = Category::select('id', 'name')->orderBy('name', 'asc')->get();
        $data['days'] = Days::select('id','name')->get();
        $data['destination'] = Destination::select('id','name','slug')->get();
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(PostAddValidation $request)
    {
        
        $getcid = $request->get('category');
        $getaid = Post::select('aid')->orderBy('id','desc')->take(1)->pluck('aid')->first();
        if ($getaid==null){
            $newaid = 1;
        }else {
            $newaid = $getaid + 1;
        }

        if($request->hasFile('image')){
            parent::checkFolderExist();

            $image = $request->file('image');
            $image_name = rand(4747, 9999).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path,$image_name);
        }


        $descount =0;
        $html = '';
        $letmecount = count($request->get('destination'));
        foreach ($request->get('destination') as $des){
            $descount++;
            $dest = Destination::select('name')->where('id',$des)->first();
            if ($letmecount==$descount){
                $html .= $dest->name;
            }else{
                $html .= $dest->name.',';
            }


        }

        $myresponse = Post::create([
            'cid' => 0,
            'name' => $request->get('name'),
            'detail' => $request->get('description'),
            'image' => isset($image_name) ? $image_name : '',
            'tags' => $request->get('tag'),
            'slug' => str_slug($request->get('name')),
            'status' => 'y',
            'aid' => $newaid,
            'days' => $request->get('days'),
            'price' => $request->get('price'),
            'destination' => $html
        ]);




        foreach ($getcid as $cid) {
            Articlecid::create([
               'aid'=>$newaid,
                'cid' => $cid
            ]);


        }

        foreach ($request->get('destination') as $mydes){
            Despost::create([
                'pid' => $myresponse->aid,
                'did' => $mydes
            ]);
        }



        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        $data = [];
        if (!$data['row'] = Post::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['menu'] = Category::select('id', 'name')->orderBy('name', 'asc')->get();
        $data['days'] = Days::select('id','name')->get();
        $data['destination'] = Destination::select('id','name','slug')->get();
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(PostEditValidation $request, $id)
    {

        if (!$page = Post::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        if($request->hasFile('image')){
            $image_path = $this->folder_path.$page->image;

            if (File::exists($image_path)) {
                File::Delete($image_path);
            }
            parent::checkFolderExist();

            $image = $request->file('image');
            $image_name = rand(4747, 9999).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path,$image_name);
        }

        $aid = $request->get('article');
        $getcid = $request->get('category');

        if (Articlecid::select('id')->where('aid',$aid)->count()!==0) {
            Articlecid::where('aid', $aid)->delete();
        }

        $counter =0;
        foreach ($getcid as $cid) {
            $counter++;
            Articlecid::create([
                'aid' => $aid,
                'cid'=>$cid
            ]);

        }

        if (Despost::select('id')->where('pid',$aid)->count()!==0) {
            Despost::where('pid', $aid)->delete();
        }

        foreach ($request->get('destination') as $mydes){
            Despost::create([
                'pid' => $aid,
                'did' => $mydes
            ]);
        }

        $descount =0;
        $html = '';
        $letmecount = count($request->get('destination'));
        foreach ($request->get('destination') as $des){
            $descount++;
            $dest = Destination::select('name')->where('id',$des)->first();
            if ($letmecount==$descount){
                $html .= $dest->name;
            }else{
                $html .= $dest->name.',';
            }


        }

        $myresponse=Post::where('aid',$aid)->update([
            'name' => $request->get('name'),
            'detail' => $request->get('description'),
            'image' => isset($image_name)?$image_name:$page->image,
            'tags' => $request->get('tag'),
            'days' => $request->get('days'),
            'price' => $request->get('price'),
            'destination' => $html
        ]);




        $request->session()->flash('message', $this->view_title.' updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (!$page = Post::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row
        $aid = Post::select('aid')->where('id',$page->id)->first();
        if ($page->image) {
            // remove old image
            if (File::exists($this->folder_path.$page->image)) {
                File::delete($this->folder_path.$page->image);
            }
        }

        Articlecid::where('aid',$aid->aid)->delete();
        $page->where('aid',$aid->aid)->delete();
        $request->session()->flash('message', $this->view_title.' deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function info(Request $request, $id)
    {
        $data = [];
        $data['person'] = Person::select('id','name')->get();
        $data['id'] = $id;
        return view(parent::loadDefaultVars($this->view_path.'.info'), compact('data'));
    }

    public function infostore(Request $request, $id)
    {
        $pcount =0;
        $html = '';
        $letmecount = count($request->get('id'));
        foreach ($request->get('id') as $des){
            $pcount++;
            $less = $pcount-1;
            if ($request->get('info')[$less]!=null && $request->get('info')[$less]!=0) {
                $dest = Person::select('name')->where('id', $des)->first();
                if ($letmecount == $pcount) {
                    $html .= $dest->name;
                } else {
                    $html .= $dest->name . ',';
                }
            }
        Post::where('id',$id)->update([
           'person' => $html
        ]);

        }

        if (Pperson::select('id')->where('aid',$id)->count()!==0){
            Pperson::where('aid',$id)->delete();
        }
        $counter = 0;
        foreach ($request->get('id') as $item){
            $counter++;
            $no = $counter-1;
            if ($request->get('info')[$no]==""){
                $price=0;
            }else{
                $price=$request->get('info')[$no];
            }

            if ($request->get('per')[$no]==""){
                $per=0;
            }else{
                $per=$request->get('per')[$no];
            }
                Pperson::create([
                    'aid' => $id,
                    'pid' => $item,
                    'price' => $price,
                    'per' => $per
                ]);


        }
        $request->session()->flash('message', 'Prices added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function suggest(Request $request, $id, $slug)
    {
        if ($slug=='y'){
            $value='n';
        }else{
            $value='y';
        }


        Post::where('aid',$id)->update([
            'suggested' => $value
        ]);

        $request->session()->flash('message', 'Action Completed Successfully');
        return redirect()->route($this->base_route.'.index');
    }

   



}