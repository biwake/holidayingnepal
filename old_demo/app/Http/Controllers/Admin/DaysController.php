<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Category\CategoryAddValidation;
use App\Http\Requests\category\CategoryEditValidation;
use App\Http\Requests\Days\DaysAddValidation;
use App\Http\Requests\Days\DaysEditValidation;
use App\Models\Category;
use App\Models\Days;
use DB, File;
use Illuminate\Http\Request;

class DaysController extends AdminBaseController
{
    protected $base_route = 'admin.days';
    protected $view_path = 'admin.days';
    protected $view_title = 'Days';
    protected $folder_name = 'days';
    protected $folder_path;

    public function __construct()
    {
        // parent::__construct();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Days::select('id','name')->orderBy('id','desc')->paginate(10);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(DaysAddValidation $request)
    {
       Days::create([
           'name'   => $request->get('name')
        ]);

        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        $data = [];
        if (!$data['row'] = Days::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(DaysEditValidation $request, $id)
    {
        if (!$page = Days::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $page->update([
            'name'   => $request->get('name')
        ]);

        // update page Set menu_id = 1, title = 'title'.....
        // where id = 4;


        $request->session()->flash('message', $this->view_title.' updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (!$page = Days::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $page->delete();
        $request->session()->flash('message', $this->view_title.' deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}