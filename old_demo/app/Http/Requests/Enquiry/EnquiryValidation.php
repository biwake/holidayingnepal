<?php

namespace App\Http\Requests\Enquiry;

use Illuminate\Foundation\Http\FormRequest;

class EnquiryValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|min:5',
            'address'              =>  'required|min:5',
            'mobileno'              =>  'required|min:10',
            'country'              =>  'required',
            'email'              =>  'required',
            'person'              =>  'required',
            'perperson'              =>  'required',
            'btnnoofperson'              =>  'required',
            'totalprice'              =>  'required',
            'msg'              =>  'required',
        ];
    }

    public function messages() {
        return [
            'name.required'                => 'Please, input Name.',
            'name.min'                     => 'Please, input name min 5 characters.',
            'address.required'                => 'Please, input Address.',
            'country.required'                => 'Please, input Country.',
            'mobileno.required'                => 'Please, input Mobile No.',
            'email.required'                => 'Please, input Email Address.',
            'person.required'                => 'Person is required',
            'perperson.required'                => 'Per Person amount is required',
            'btnnoofperson.required'                => 'Please input no of person',
            'totalprice.required'                => 'Total Amount is required',
            'msg.required'                => 'Please write some message for us',
            'address.min'                     => ' Adress must be min 5 characters.',
            'mobileno.min'                     => 'Mobile No must be min 10 characters.',

        ];
    }
}
