<?php

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class PostEditValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|min:5',
            'category'              =>  'required',
            'description'              =>  'required',
            'tag'              =>  'required',
            'destination'              =>  'required',
        ];
    }

    public function messages() {
        return [
            'name.required'                => 'Please, Add Name.',
            'name.min'                     => 'Please, Add min 5 characters.',
            'description.required'                => 'Please, add description.',
            'tag.required'                => 'Please, add at least one tag.',
            'destination.required'                => 'Please, select at least one destination.',
        ];
    }
}
