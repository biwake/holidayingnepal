<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['name', 'slug', 'cat', 'description', 'created_at', 'updated_at','type'];



}
