<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Imagetbl extends Model
{
    protected $table = 'imagetbl';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['image', 'created_at', 'updated_at'];



}
