<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Despost extends Model
{
    protected $table = 'despost';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['pid','did', 'created_at', 'updated_at'];



}
