<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $table = 'budget';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['name', 'start', 'end', 'created_at', 'updated_at'];



}
