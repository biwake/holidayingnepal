<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'person';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['name', 'created_at', 'updated_at','start','end'];



}
