<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Enquiry extends Model
{
    protected $table = 'enquiry';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['pid', 'name', 'address', 'country', 'mobile', 'email', 'contact', 'personid', 'person', 'per', 'total', 'msg', 'created_at', 'updated_at'];



}
