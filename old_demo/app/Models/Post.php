<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'article';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['cid', 'name', 'detail', 'image', 'tags', 'slug','status','aid', 'created_at', 'updated_at','days','price','destination','type','person','suggested'];

    public function category()
    {
        return $this->hasMany('App\Models\Category', 'id');
    }



}
