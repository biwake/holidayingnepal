<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    protected $table = 'price';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['name', 'start', 'end', 'created_at', 'updated_at'];



}
