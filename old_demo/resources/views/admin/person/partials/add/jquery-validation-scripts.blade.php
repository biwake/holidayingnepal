<script src="{{ asset('assets/admin/plugins/jquery-validation/dist/jquery.validate.js') }}"></script>
<script>
    $(document).ready(function () {

        $("#listing-form").validate({

            rules: {
                name: {
                    required: true,
                    minlength: 3
                },


            },
            messages: {
                name: {
                    required: "Please input a name.",
                    minlength: "Your name must be at least 3 characters long"
                },

            },

        });

    });
</script>
