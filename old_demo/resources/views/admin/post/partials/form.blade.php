<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="space-4"></div>
<div class="col-sm-8">


<div class="form-group">

    <div class="col-sm-12">
        <input type="text" name="name" class="col-sm-12" id="name" value="{{ ViewHelper::getData('name', isset($data['row'])?$data['row']:[]) }}" placeholder="Title" class="col-xs-10 col-sm-5">
    </div>

</div>
<div class="space-4"></div>



<div class="form-group">

    <div class="col-sm-12">
        <textarea name="description" class="col-sm-12" rows="10" id="description" class="col-xs-10 col-sm-5">{{ ViewHelper::getData('detail', isset($data['row'])?$data['row']:[]) }}</textarea>

        <script>
            CKEDITOR.replace( 'description', {
                filebrowserImageBrowseUrl: '/laravel-filemanager?type=Images',
                filebrowserImageUploadUrl: '/laravel-filemanager/upload?type=Images&_token={{csrf_token()}}',
                filebrowserBrowseUrl: '/laravel-filemanager?type=Files',
                filebrowserUploadUrl: '/laravel-filemanager/upload?type=Files&_token={{csrf_token()}}'
            });
        </script>
    </div>
</div>

    <div class="space-4"></div>

    <div class="col-sm-3">
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1" style="text-align: left;"> Duration </label>
            <div class="col-sm-12">
                <select name="days" class="col-sm-12">
                    <option value="">Select Days</option>
                    @foreach($data['days'] as $days)
                        <option value="{{ $days->id }}" {{ isset($data['row']) && $data['row']->days==$days->id?'selected':''  }}>{{ $days->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>

    <div class="col-sm-3">
        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1" style="text-align: left;"> Price($) </label>
            <div class="col-sm-12">
                <input type="number" name="price" class="col-sm-12" placeholder="500 No any symbol allowed." style="float:left;" value="{{ ViewHelper::getData('price', isset($data['row'])?$data['row']:[]) }}">
            </div>
        </div>
    </div>

    <div class="col-sm-6">
        <div class="form-group">
            <label class="col-sm-12 control-label no-padding-right" for="form-field-1" style="text-align: left;"> Tags </label>
            <div class="col-sm-12">
                <input type="text" name="tag" class="col-sm-12" style="float:left;" value="{{ ViewHelper::getData('tags', isset($data['row'])?$data['row']:[]) }}">
            </div>
        </div>
    </div>

</div>

<div class="col-sm-4">

    <div class="col-sm-12">
        <h1 style="margin: 0; padding: 0; font-size: 1.2em; padding-bottom: 10px; border-bottom: 1px solid #e5e5e5;">Category</h1>
    <div class="form-group" style="margin-top: 10px; overflow: auto; height: 200px;">
        @foreach($data['menu'] as $name )
        <div class="col-sm-12">
            <input type="checkbox" name="category[]"  class="col-sm-1" value="{{ $name->id }}" {{ isset($data['row'])?MyHelper::getcategoryid($data['row']->aid,$name->id):'' }} style="float:left;"><p style="margin-top:2px;">{{ $name->name }}</p>
        </div>
            @endforeach
        <div id="question"></div>
    </div>

    </div>
    <div class="space-4"></div>

    <div class="col-sm-12">
        <h1 style="margin: 0; padding: 0; font-size: 1.2em; padding-bottom: 10px; border-bottom: 1px solid #e5e5e5;">Destination</h1>
        <div class="form-group" style="margin-top: 10px; overflow: auto; height: 170px;">
            @foreach($data['destination'] as $destination )
                <div class="col-sm-12">
                    <input type="checkbox" name="destination[]"  class="col-sm-1" value="{{ $destination->id }}" {{ isset($data['row'])?MyHelper::getdestinationid($data['row']->aid,$destination->id):'' }} style="float:left;"><p style="margin-top:2px;">{{ $destination->name }}</p>

                </div>
            @endforeach
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label no-padding-right" for="form-field-1" style="text-align: left;"> Image </label><br>
            @if(isset($data['row']))
                <img src="{{ url('/images/post/').'/'.$data['row']->image }}" style="height: 100px; float: left;"><div style="clear: both"></div><br>
            @endif
            <div class="col-sm-12">
                <input type="file" name="image" class="col-sm-12" style="float:left;">
            </div>

        </div>

    </div>
    <div class="space-4"></div>

</div>
<div class="clearfix"></div>




