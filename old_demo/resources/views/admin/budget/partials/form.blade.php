<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Title </label>

    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ ViewHelper::getData('name', isset($data['row'])?$data['row']:[]) }}" placeholder="Name" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> From($) </label>

    <div class="col-sm-9">
        <input type="number" name="start" value="{{ ViewHelper::getData('start', isset($data['row'])?$data['row']:[]) }}" placeholder="100" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> To($) </label>

    <div class="col-sm-9">
        <input type="text" name="end" value="{{ ViewHelper::getData('end', isset($data['row'])?$data['row']:[]) }}" placeholder="200" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>



