<tr>
    <td>
        <select name="attribute_group[]">
            @if ($data['attribute_group']->count() > 0)
            @foreach($data['attribute_group'] as $attribute_group)
                <option value="{{ $attribute_group->id }}"
                {{ ($attribute_group->id == $product_attribute->product_attribute_group_id)?'selected="selected"':'' }}
                >{{ $attribute_group->title }}</option>
            @endforeach
            @else
                <option value="0">No Attribute Group</option>
            @endif
        </select>
    </td>
    <td>
        <select name="attribute[]">
            @if ($data['attributes']->count() > 0))
            @foreach($data['attributes'] as $attribute)
                <option value="{{ $attribute->id }}"
                        {{ ($attribute->id == $product_attribute->product_attribute_id)?'selected="selected"':'' }}
                >{{ $attribute->title }}</option>
            @endforeach
            @else
                <option value="0">No Attribute</option>
            @endif
        </select>
    </td>
    <td><input type="text" name="value[]" value="{{ $product_attribute->value }}"></td>
    <td>
        <div class="btn-group">

            <a href="javascript:void(0);" class="btn btn-xs btn-danger remove-btn">
                <i class="icon-trash bigger-120"></i>
            </a>
        </div>

    </td>
</tr>