<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="{{ route('admin.dashboard') }}">Dashboard</a>
    </li>

    <li>
        <a href="{{ $base_route.'.index' }}">{{ $view_title }} </a>
    </li>

    @if (isset($data))
        <li class="active">Edit </li>
    @else
        <li class="active">Add</li>
    @endif

</ul><!-- .breadcrumb -->