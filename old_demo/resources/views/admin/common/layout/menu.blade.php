<div class="sidebar" id="sidebar">
    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="icon-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="icon-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="icon-group"></i>
            </button>

            <button class="btn btn-danger">
                <i class="icon-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">

        <li class="{{ Request::is('dashboard')?'active':'' }}">
            <a href="{{ route('admin.image.index') }}">
                <i class="icon-dashboard"></i>
                <span class="menu-text"> Image Manager </span>
            </a>
        </li>

        @if(Auth::user()->type=="admin")
            <li class="{{ Request::is('boss/user*')?'active':'' }}">
                <a href="#" class="dropdown-toggle">
                    <i class="icon-user"></i>
                    <span class="menu-text"> User Manager  </span>

                    <b class="arrow icon-angle-down"></b>
                </a>

                <ul class="submenu">
                    <li class="{{ Request::is('boss/user')?'active':'' }}">
                        <a href="{{ route('admin.user.index') }}">
                            <i class="icon-double-angle-right"></i>
                            List
                        </a>
                    </li>

                    <li class="{{ Request::is('boss/user/add')?'active':'' }}">
                        <a href="{{ route('admin.user.add') }}">
                            <i class="icon-double-angle-right"></i>
                            Add
                        </a>
                    </li>
                </ul>
            </li>
        @endif

        @if(Auth::user()->type=="admin")
        <li class="{{ Request::is('boss/category*')?'active':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Category Manager  </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('boss/category/home')?'active':'' }}">
                    <a href="{{ route('admin.category.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li class="{{ Request::is('boss/category/add')?'active':'' }}">
                    <a href="{{ route('admin.category.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if(Auth::user()->type=="admin")
        <li class="{{ Request::is('boss/days*')?'active':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Days Manager  </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('boss/days/home')?'active':'' }}">
                    <a href="{{ route('admin.days.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li class="{{ Request::is('boss/days/add')?'active':'' }}">
                    <a href="{{ route('admin.days.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>
        @endif

        <li class="{{ Request::is('boss/destination*')?'active':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Destination Manager  </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('boss/destination')?'active':'' }}">
                    <a href="{{ route('admin.destination.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li class="{{ Request::is('boss/destination/add')?'active':'' }}">
                    <a href="{{ route('admin.destination.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>

        @if(Auth::user()->type=="admin")
        <li class="{{ Request::is('boss/setting/home*')?'active':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Home Manager  </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('boss/setting/home')?'active':'' }}">
                    <a href="{{ route('admin.setting.home.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li class="{{ Request::is('boss/setting/home/add')?'active':'' }}">
                    <a href="{{ route('admin.setting.home.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if(Auth::user()->type=="admin")
        <li class="{{ Request::is('boss/price*')?'active':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Price Manager  </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('boss/price/list')?'active':'' }}">
                    <a href="{{ route('admin.price.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li class="{{ Request::is('boss/price/add')?'active':'' }}">
                    <a href="{{ route('admin.price.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if(Auth::user()->type=="admin")
        <li class="{{ Request::is('boss/person*')?'active':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Person Manager  </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('boss/person/list')?'active':'' }}">
                    <a href="{{ route('admin.person.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li class="{{ Request::is('boss/person/add')?'active':'' }}">
                    <a href="{{ route('admin.person.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>
        @endif

        <li class="{{ Request::is('boss/post*')?'active':'' }}">
            <a href="#" class="dropdown-toggle">
                <i class="icon-user"></i>
                <span class="menu-text"> Program Manager  </span>

                <b class="arrow icon-angle-down"></b>
            </a>

            <ul class="submenu">
                <li class="{{ Request::is('boss/post/list')?'active':'' }}">
                    <a href="{{ route('admin.post.index') }}">
                        <i class="icon-double-angle-right"></i>
                        List
                    </a>
                </li>

                <li class="{{ Request::is('boss/post/add')?'active':'' }}">
                    <a href="{{ route('admin.post.add') }}">
                        <i class="icon-double-angle-right"></i>
                        Add
                    </a>
                </li>
            </ul>
        </li>



       </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
        <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
        try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>