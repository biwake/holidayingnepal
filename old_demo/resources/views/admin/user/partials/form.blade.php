<div class="space-4"></div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Name </label>

    <div class="col-sm-9">
        <input type="text" name="name" id="name" value="{{ ViewHelper::getData('name', isset($data['row'])?$data['row']:[]) }}" placeholder="Name" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Type </label>

    <div class="col-sm-9">
        <select class="col-xs-10 col-sm-5" name="type">
            <option value="page" {{ isset($data['row']) && $data['row']->type=='staff'?'selected':''  }}>Staff</option>
            <option value="about" {{ isset($data['row']) && $data['row']->type=='admin'?'selected':''  }}>Admin</option>
        </select>

    </div>
</div>

<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Email </label>

    <div class="col-sm-9">
        <input type="text" name="email" value="{{ ViewHelper::getData('email', isset($data['row'])?$data['row']:[]) }}" placeholder="Email" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> Password </label>

    <div class="col-sm-9">
        <input type="password" name="password" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="space-4"></div>


