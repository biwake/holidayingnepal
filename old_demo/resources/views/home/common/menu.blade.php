<div class="header">
    <div class="header-nav" style="border-bottom: 1px solid #bd0c16;">
        <div class="container">
            <div class="logo">
                <a href="{{ route('home.home.index') }}" style="font-size:26px;">
                    Holidaying Nepal
                    <!--<img src="images/logo1.png" alt="" />--></a>
            </div>
            <div class="navigation">
                <nav class="navbar navbar-default">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"> </span>
                            <span class="icon-bar"> </span>
                            <span class="icon-bar"> </span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">



                            @foreach($data['menu'] as $menu)
                            {{ ViewHelper::getmenu($menu->id) }}
                                @endforeach


                        </ul>
                        <div class="clearfix"> </div>
                    </div><!-- /.navbar-collapse -->
                </nav>
            </div>
        </div>
    </div>
</div>
<!-- script-for-menu -->
<script>
    $("span.menu").click(function(){
        $(".top-nav ul").slideToggle("slow" , function(){
        });
    });
</script>