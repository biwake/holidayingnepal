<div class="footer">
    <div class="container">
        <div class="col-md-3 ftr_navi ftr">
            <h3>Navigation</h3>
            <ul>
                <li><a href="{{ route('home.home.index') }}">Home</a></li>
                <li><a href="{{ route('about.index') }}">About</a></li>
                <li><a href="{{ route('contact.index') }}">Contact</a></li>
            </ul>
        </div>
        <div class="col-md-3 ftr_navi ftr">
            <h3>Things to do</h3>
            <ul>
                @foreach($data['foot'] as $foot)
                <li><a href="{{ route('category.index', ['slug'=>$foot->slug]) }}">{{ $foot->name }}</a></li>
                @endforeach
            </ul>
        </div>
        <div class="col-md-3 get_in_touch ftr">
            <h3>Get In Touch</h3>
            <p>Thamel</p>
            <p>Kathmandu, Nepal</p>
            <p>+977 01-4-321345</p>
            <a href="#">hello@holidayingnepal.com</a>
        </div>
        <div class="col-md-3 ftr-logo">
            <a href="index.html"><h3>Holidaying Nepal</h3></a>
            <p>© 2017 Holidaying Nepal. All rights  Reserved | Powered by &nbsp;<a href="http://onlinemultimediainfo.com">Online Multimedia</a></p>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>