<div id="row-wrapper">
@include('home.common.left')

    <div class="col-md-9 badges-Info1">
        <div class="grid" style="padding: 0;margin-top: 20px;" >
            <p style="color: #bd0c16;"> Search Result By <br>
                <label style="color: #023e84;">
                @if(isset($data['search_queries']['duration']))Duration: {{ $data['search_queries']['duration'] }},@endif
                        @if(isset($data['search_queries']['price']))Price: {{ $data['search_queries']['price'] }}@endif
                </label>
            </p>
        </div>
        @if(count($data['rows'])==0)
            <h3 style="color: #bd0c16;">Sorry, No any travel package found.</h3>
        @else
            @foreach($data['rows'] as $package)
                <div class="col-md-12" style="margin:15px 0px; padding: 15px 0; border: 1px solid #bd0c16;">
                    <div class="col-md-4">
                        <a href="{{ ViewHelper::getsearchlink($package->cid,$package->slug) }}"><img style="height: 160px; width: 213px;" src="{{ asset('images/post/'.$package->image) }}" alt=""></a>
                    </div>


                    <div class="col-md-8">
                        <h3 style="text-align: left;"><a href="{{ ViewHelper::getsearchlink($package->cid,$package->slug) }}" style="text-transform: capitalize;">{{ $package->name }}</a></h3>
                        <p style="text-align: left; margin-top: 5px;"><a href="#">{{ $package->days }}</a></p>
                        <p style="text-align: left; margin-top: 5px;">To: {{ ViewHelper::getdestination($package->aid) }}</p>
                        <button type="button" name="{{ $package->slug }}" style="float:right;" class="btn btn-info btn-lg" id="{{ $package->name }}" data-content="check" data-toggle="modal" data-target="#myModal">Enquire Now</button>
                        <h3 style="text-align: left; margin-top: 5px; color: #bd0c16; @if(isset($data['people']))font-size: 18px;@endif">Starting From ${{ $package->price }}</h3>


                    </div>


                    <div class="clearfix"> </div>

                </div>
            @endforeach


        @endif

        @include('home.destination.include.modal')
    </div>
</div>

@include('home.inner.script')
@include('home.partials.jquery-validation-scripts')