@extends('home.common.layout')
@section('page_title') {{ $data['slug']->name }} @endsection
@section('keyword') @endsection
@section('description') @endsection
@section('content')

    <div class="container">

<div class="grid" id="grid-row-wrapper">
    <div id="row-wrapper">
        @include('home.common.left')

    <div class="col-md-9 badges-Info1">
        <div class="grid" >
        <h4 style="margin-bottom: 0;"> {{ $data['slug']->name }}</h4>
        </div>
        @if(count($data['rows'])==0)
            <h3 style="color: #bd0c16;">Sorry, No any travel package found.</h3>
            @else
            @foreach($data['rows'] as $package)
        <div class="col-md-12" style="margin:15px 0px; padding: 15px 0; border: 1px solid #bd0c16;">
            <div class="col-md-4">
                <a href="{{ route('category.detail', ['id'=>$data['slug']->slug,'slug'=>$package->slug]) }}"><img style="height: 160px; width: 213px;" src="{{ asset('images/post/'.$package->image) }}" alt=""></a>
            </div>


                <div class="col-md-8">
                    <h3 style="text-align: left;"><a href="{{ route('category.detail', ['id'=>$data['slug']->slug,'slug'=>$package->slug]) }}" style="text-transform: capitalize;">{{ $package->name }}</a></h3>
                    <p style="text-align: left; margin-top: 5px;"><a href="#">{{ $package->days }}</a></p>
                    <p style="text-align: left; margin-top: 5px;">To: {{ ViewHelper::getdestination($package->aid) }}</p>
                    <button type="button" name="{{ $package->slug }}" style="float:right;" class="btn btn-info btn-lg" id="{{ $package->name }}" data-content="check" data-toggle="modal" data-target="#myModal">Enquire Now</button>
                    <h3 style="text-align: left; margin-top: 5px; color: #bd0c16;">Starting From ${{ $package->price }}</h3>


                </div>


                <div class="clearfix"> </div>

        </div>
            @endforeach



                {{ $data['rows']->links() }}
            @endif

        @include('home.destination.include.modal')



    </div>
</div>
    @include('home.inner.script')
</div>


    </div>
    @endsection

@section('page_script')

    @include('home.partials.jquery-validation-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    @endsection