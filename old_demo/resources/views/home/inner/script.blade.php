<script type="text/javascript">

    $(document).ready(function () {

        $('#loadingimage').hide();
        $('#city-btn').change(function () {
            var geturl = "{{ URL::route('category.search') }}";
            var city = $('#city-btn').val();
            var duration = $("input[name=durationbtn]:checked").val();
            var person = $('#person-btn').val();
            var budget = $("input[name=budget]:checked").val();
            var price = $("input[name=price]:checked").val();
            $.ajax({
                url: geturl,
                method: 'POST',
                data: {
                    '_token' : "{{ csrf_token() }}", city : city, duration: duration, person: person, budget: budget, price: price,
                },
                success: function (response) {
                    $('#row-wrapper').remove();
                    $('#grid-row-wrapper').append(response);
                    $('#loadingimage').hide();
                }
            });

        });

        $("input[name=durationbtn]").change(function () {
            var geturl = "{{ URL::route('category.search') }}";
            var city = $('#city-btn').val();
            var duration = $("input[name=durationbtn]:checked").val();
            var person = $('#person-btn').val();
            var budget = $("input[name=budget]:checked").val();
            var price = $("input[name=price]:checked").val();
            $.ajax({
                url: geturl,
                method: 'POST',
                data: {
                    '_token' : "{{ csrf_token() }}", city : city, duration: duration, person: person, budget: budget, price: price,
                },
                success: function (response) {
                    $('#row-wrapper').remove();
                    $('#grid-row-wrapper').append(response);
                    $('#loadingimage').hide();
                }
            });

        });

        $('#person-btn').change(function () {
            var geturl = "{{ URL::route('category.search') }}";
            var city = $('#city-btn').val();
            var duration = $("input[name=durationbtn]:checked").val();
            var person = $('#person-btn').val();
            var budget = $("input[name=budget]:checked").val();
            var price = $("input[name=price]:checked").val();
            $.ajax({
                url: geturl,
                method: 'POST',
                data: {
                    '_token' : "{{ csrf_token() }}", city : city, duration: duration, person: person, budget: budget, price: price,
                },
                success: function (response) {
                    $('#row-wrapper').remove();
                    $('#grid-row-wrapper').append(response);
                    $('#loadingimage').hide();
                }
            });

        });

        $("input[name=budget]").change(function () {
            var geturl = "{{ URL::route('category.search') }}";
            var city = $('#city-btn').val();
            var duration = $("input[name=durationbtn]:checked").val();
            var person = $('#person-btn').val();
            var budget = $("input[name=budget]:checked").val();
            var price = $("input[name=price]:checked").val();
            $.ajax({
                url: geturl,
                method: 'POST',
                data: {
                    '_token' : "{{ csrf_token() }}", city : city, duration: duration, person: person, budget: budget, price: price,
                },
                success: function (response) {
                    $('#row-wrapper').remove();
                    $('#grid-row-wrapper').append(response);
                    $('#loadingimage').hide();
                }
            });

        });

        $("input[name=price]").change(function () {
            var geturl = "{{ URL::route('category.search') }}";
            var city = $('#city-btn').val();
            var duration = $("input[name=durationbtn]:checked").val();
            var person = $('#person-btn').val();
            var budget = $("input[name=budget]:checked").val();
            var price = $("input[name=price]:checked").val();
            $.ajax({
                url: geturl,
                method: 'POST',
                data: {
                    '_token' : "{{ csrf_token() }}", city : city, duration: duration, person: person, budget: budget, price: price,
                },
                success: function (response) {
                    $('#row-wrapper').remove();
                    $('#grid-row-wrapper').append(response);
                    $('#loadingimage').hide();
                }
            });

        });

    });

</script>