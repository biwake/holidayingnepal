@extends('home.common.layout')
@section('page_title') Home @endsection
@section('keyword') @endsection
@section('description') @endsection
@section('content')
    @include('home.common.banner')
    @foreach($data['position'] as $position)
    <div class="container">
        <div class="grid">
            <h4 style="margin-top:10px;">{{ $position->title }}</h4>
{{ ViewHelper::getarticle($position->cid, $position->title) }}
            <div class="clearfix"> </div>
        </div>
    </div>
    @endforeach
    @endsection

@section('page_script')
    @include('home.partials.jquery-validation-scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            /*
             var defaults = {
             containerID: 'toTop', // fading element id
             containerHoverID: 'toTopHover', // fading element hover id
             scrollSpeed: 1200,
             easingType: 'linear'
             };
             */
            $().UItoTop({ easingType: 'easeOutQuart' });
        });
    </script>
    <a href="#to-top" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
    @endsection