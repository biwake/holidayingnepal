

@foreach(json_decode($data)->row as $grid)
<div class="grid_box">
    <figure class="effect-julia">
        <img src="{{ asset('images/post/'.$grid->image) }}" alt="img21"/>


    </figure>
    <h2 style="width: 100%; text-align: left;"><a href="{{ route('category.detail', ['id'=>json_decode($data)->slug->slug,'slug'=>$grid->slug]) }}">{{ $grid->name }}</a></h2>
    <p style="float: left; margin-left:5px;">{{ $grid->days }}</p>
    <div class="clearfix"></div>
    <h2><a href="#"> ${{ $grid->price }}</a></h2>
    <div class="clearfix"></div>
    <p style="width: 215px; background: #bd0c16; margin-left: 5px; margin-bottom: 10px; color: #ffffff; padding:10px 0px;">Enquire Now</p>
</div>
    @endforeach