
<style type="text/css">
    .error{
        width:100%;
    }
</style>

<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title edit-content" style="font-size: 16px; margin: 0;"></h4>
            </div>
            <div class="modal-body">
                <p class=""></p>
                <div id="loadingimages" style="position: absolute;width: 62%;"><img src="{{ asset('images/gears.gif') }}" style="    position: relative;width: 100%;left: 20%;"> </div>
                <div id="errorTxt" style="color:red;"></div>
                <form method="post" id="listing-form"  action="{{ route('enquiry.mail.index') }}" >
                    <input type="hidden" class="myreplicate" name="package">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div style="width: 49%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Name</label>
                        <input type="text" name="name" style="width: 100%; float: left;">
                    </div>
                    <div style="width: 49%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Address</label>
                        <input type="text" name="address" style="width: 100%; float: left;">
                    </div>

                    <div style="width: 49%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Country</label>
                        <input type="text" name="country" style="width: 100%; float: left;" value="{{ ViewHelper::letcheck() }}" readonly>
                    </div>

                    <div style="width: 49%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Mobile No</label>
                        <input type="number" name="mobileno" style="width: 100%; float: left;">
                    </div>
                    <div style="width: 49%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Email Address</label>
                        <input type="text" name="email" style="width: 100%; float: left;">
                    </div>
                    <div style="width: 49%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Contact No(Optional)</label>
                        <input type="number" name="contact" style="width: 100%; float: left;">
                    </div>

                    <div style="width: 30%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Person</label>
                        @if(isset($data['type']) && $data['type']=='home')
                        <select style="float: left; width: 100%;" id="person-btn-enquire" name="person">
                            <option value="">Select Person</option>
                                @foreach($data['people'] as $people)
                                <option value="{{ $people->name }}">{{ $people->name }}</option>
                            @endforeach
                        </select>

                        @else
                            <select style="float: left; width: 100%;" id="person-btn-enquire" name="person">
                                <option value="">Select Person</option>
                                @foreach(json_decode($data)->people as $people)
                                    <option value="{{ $people->name }}">{{ $people->name }}</option>
                                @endforeach
                            </select>

                                @endif
                    </div>
                    <div id="price-row-wrapper">
                        <div id="price-wrapper">
                    <div style="width: 20%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">No. Of Person</label>
                        <input type="number" name="btnnoofperson"  style="width: 100%; float: left;" id="btn-no-of-person" readonly>
                    </div>


                    <div style="width: 26%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Price Per Person($)</label>
                        <input type="text" name="perperson" style="width: 100%; float: left;" id="btn-price-per-person" readonly>
                    </div>

                    <div style="width: 20%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Total Price($)</label>
                        <input type="text" name="totalprice" style="width: 100%; float: left;" id="btn-total-price" readonly>
                    </div>
                    </div>
                    </div>

                    <div style="width: 47%; float: left; margin-right: 1%;">
                        <label style="width: 100%; text-align: left;">Message</label>
                        <textarea rows="5" cols="77" name="msg"></textarea>
                    </div>

                    <div style="clear:both"></div>

                    <div style="width: 55%; float: left; margin-right: 1%;">

                        <input type="submit" style="float:right; background: #bd0c16; color: #ffffff; border: none; padding: 10px 20px;" value="Submit">
                    </div>

                    <label>

                    </label>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
            @include('home.destination.include.script')
        </div>


    </div>
</div>

