<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Days extends Model
{
    protected $table = 'days';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['name', 'created_at', 'updated_at'];



}
