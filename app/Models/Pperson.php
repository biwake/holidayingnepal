<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pperson extends Model
{
    protected $table = 'pperson';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['aid', 'pid', 'price', 'created_at', 'updated_at', 'per'];



}
