<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Destination extends Model
{
    protected $table = 'destination';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['name','slug', 'created_at', 'updated_at'];



}
