<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Articlecid extends Model
{
    protected $table = 'article_cat';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['aid', 'cid', 'created_at', 'updated_at'];



}
