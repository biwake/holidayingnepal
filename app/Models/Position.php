<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'position';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['cid', 'title', 'pos', 'created_at', 'updated_at'];



}
