<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $table = 'about';

    // protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['about', 'created_at', 'updated_at'];



}
