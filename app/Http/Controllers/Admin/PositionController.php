<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Category\CategoryAddValidation;
use App\Http\Requests\category\CategoryEditValidation;
use App\Http\Requests\Position\PositionAddValidation;
use App\Http\Requests\Position\PositionEditValidation;
use App\Models\Category;
use App\Models\Position;
use DB;
use Illuminate\Http\Request;

class PositionController extends AdminBaseController
{
    protected $base_route = 'admin.setting.home';
    protected $view_path = 'admin.home';
    protected $view_title = 'Position';
    protected $folder_name = 'position';
    protected $folder_path;

    public function __construct()
    {
        // parent::__construct();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Position::select('position.id as id','category.name as name','position.title as title','position.pos as position')
            ->leftjoin('category', 'category.id', '=', 'position.cid')
            ->orderBy('pos','asc')->paginate(10);

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];
        $data['menu'] = Category::select('id', 'name')->orderBy('name', 'asc')->get();
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(PositionAddValidation $request)
    {
            if (Position::select('id')->where('pos',$request->get('position'))->count()==0){
                Position::create([
                    'cid'   => $request->get('parent'),
                    'title'   => $request->get('name'),
                    'pos' => $request->get('position')
                ]);
            }else{
                $getvalue = Position::select('position.cid as cid', 'category.name as name')
                    ->leftjoin('category','category.id', '=', 'position.cid')
                    ->where('pos',$request->get('position'))->first();
                $request->session()->flash('message', 'Error'. $request->get('position').' has already been assigned to '.$getvalue->name);
                return redirect()->route($this->base_route.'.index');
            }


        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        $data = [];
        if (!$data['row'] = Position::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        $data['menu'] = Category::select('id', 'name')->orderBy('name', 'asc')->get();
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(PositionEditValidation $request, $id)
    {
        if (Position::select('id')->where('pos',$request->get('position'))->where('id','!=',$id)->count()==0){
            if (!$page = Position::find($id))
                return redirect()->route('admin.error', ['code' => '500']);

            $page->update([
                'cid'   => $request->get('parent'),
                'title'   => $request->get('name'),
                'pos' => $request->get('position')
            ]);
        }else{
            $getvalue = Position::select('position.cid as cid', 'category.name as name')
                ->leftjoin('category','category.id', '=', 'position.cid')
                ->where('pos',$request->get('position'))->first();
            $request->session()->flash('message', 'Error'. $request->get('position').' has already been assigned to '.$getvalue->name);
            return redirect()->route($this->base_route.'.index');
        }
        // update page Set menu_id = 1, title = 'title'.....
        // where id = 4;


        $request->session()->flash('message', $this->view_title.' updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (!$page = Position::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $page->delete();
        $request->session()->flash('message', $this->view_title.' deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}