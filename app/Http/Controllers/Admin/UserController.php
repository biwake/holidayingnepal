<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Category\CategoryAddValidation;
use App\Http\Requests\Category\CategoryEditValidation;
use App\Models\Category;
use App\User;
use DB, File;
use Illuminate\Contracts\Session\Session;
use Illuminate\Http\Request;

class UserController extends AdminBaseController
{
    protected $base_route = 'admin.user';
    protected $view_path = 'admin.user';
    protected $view_title = 'User';
    protected $folder_name = 'user';
    protected $folder_path;

    public function __construct()
    {
        // parent::__construct();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = User::select('id','name','email')->orderBy('id','desc')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(CategoryAddValidation $request)
    {
       User::create([
           'name'   => $request->get('name'),
           'email' => $request->get('email'),
           'password'   => bcrypt($request->get('password')),
           'type' => $request->get('type')
        ]);

        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        $data = [];
        if (!$data['row'] = User::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(CategoryEditValidation $request, $id)
    {
        if (!$page = User::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $page->update([
            'name'   => $request->get('name'),
            'email' => $request->get('email'),
            'password'   => bcrypt($request->get('password')),
            'type' => $request->get('type')
        ]);

        // update page Set menu_id = 1, title = 'title'.....
        // where id = 4;


        $request->session()->flash('message', $this->view_title.' updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (!$page = User::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $page->delete();
        $request->session()->flash('message', $this->view_title.' deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}