<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;


use App\Http\Requests\Person\PersonAddValidation;
use App\Http\Requests\Person\PersonEditValidation;
use App\Models\Person;
use DB, File;
use Illuminate\Http\Request;

class PersonController extends AdminBaseController
{
    protected $base_route = 'admin.person';
    protected $view_path = 'admin.person';
    protected $view_title = 'Person';
    protected $folder_name = 'person';
    protected $folder_path;

    public function __construct()
    {
        // parent::__construct();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Person::select('id','name')->orderBy('id','desc')->get();

        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(PersonAddValidation $request)
    {
       Person::create([
           'name'   => $request->get('name'),
           'start'   => $request->get('start'),
           'end'   => $request->get('end'),
        ]);

        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        // get user data for $id
        $data = [];
        if (!$data['row'] = Person::find($id))
            return redirect()->route('admin.error', ['code' => '500']);
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(PersonEditValidation $request, $id)
    {
        if (!$page = Person::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        $page->update([
            'name'   => $request->get('name'),
            'start'   => $request->get('start'),
            'end'   => $request->get('end'),
        ]);

        // update page Set menu_id = 1, title = 'title'.....
        // where id = 4;


        $request->session()->flash('message', $this->view_title.' updated successfully.');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (!$page = Person::find($id))
            return redirect()->route('admin.error', ['code' => '500']);

        // remove image before deleting db row

        $page->delete();
        $request->session()->flash('message', $this->view_title.' deleted successfully.');
        return redirect()->route($this->base_route.'.index');
    }



}