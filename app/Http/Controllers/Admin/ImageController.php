<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Admin;



use App\Models\Imagetbl;
use DB, File;
use Illuminate\Http\Request;

class ImageController extends AdminBaseController
{
    protected $base_route = 'admin.image';
    protected $view_path = 'admin.image';
    protected $view_title = 'Image';
    protected $folder_name = 'image';
    protected $folder_path;

    public function __construct()
    {
        // parent::__construct();
        $this->folder_path = 'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        $data = [];
        $data['row']=Imagetbl::select('image')->where('id',1)->first();
        return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function add(Request $request)
    {

    }

    public function store(Request $request)
    {
        $page=Imagetbl::select('image')->where('id',1)->first();
        if($request->hasFile('image')){
            $image_path = $this->folder_path.$page->image;

            if (File::exists($image_path)) {
                File::Delete($image_path);
            }
            parent::checkFolderExist();

            $image = $request->file('image');
            $image_name = rand(4747, 9999).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path,$image_name);
        }




        Imagetbl::where('id',1)->update([
            'image' => $image_name
        ]);



        $request->session()->flash('message', $this->view_title.' added successfully.');
        return redirect()->route($this->base_route.'.index');
    }





}