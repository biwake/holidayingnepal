<?php
namespace App\Http\Controllers;
use App\Models\Enquiry;
use App\Models\Person;
use App\Models\Post;
use Illuminate\Support\Facades\Input;
use Mail;

use App\Http\Requests;
use App\Http\Requests\Enquiry\EnquiryValidation;

class MailController extends Controller {
    public function basic_email(EnquiryValidation $request){

        $pack = Post::select('id','name')->where('slug',$request->get('package'))->first();
        $person = Person::select('id')->where('name',$request->get('person'))->first();
        Enquiry::create([
           'pid' => $pack->id,
           'name' => $request->get('name'),
           'address' => $request->get('address'),
           'country' => $request->get('country'),
           'mobile' => $request->get('mobileno'),
           'email' => $request->get('email'),
           'contact' => $request->get('contact'),
           'personid' => $person->id,
           'person' => $request->get('btnnoofperson'),
           'per' => $request->get('perperson'),
           'total' => $request->get('totalprice'),
           'msg' => $request->get('msg'),
        ]);

        $data = array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'pid' => $pack->name,
            'address' => $request->get('address'),
            'country' => $request->get('country'),
            'mobile' => $request->get('mobileno'),
            'contact' => $request->get('contact'),
            'personid' => $request->get('person'),
            'person' => $request->get('btnnoofperson'),
            'per' => $request->get('perperson'),
            'total' => $request->get('totalprice'),
            'msg' => $request->get('msg'),
        );

        Mail::send('mail', $data, function($message) {
            $message->to('hello@infinitytours.com.np', 'India To Nepal Tour')->subject
            ('New Enquiry Request Has Been Received on india2nepaltour.com');
            $message->from('hello@india2nepaltour.com','India To Nepal Tour');
        });

        Mail::send('welcome', $data, function($message) {
            $message->to(Input::get('email'), 'India To Nepal Tour')->subject
            ('Thank you for contacting us');
            $message->from('hello@india2nepaltour.com','India To Nepal Tour');
        });



        echo "Thank you for contacting us. We will get back to you within 24 hours.";
    }

    public function html_email(){
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
            $message->to('sales@infinitytours.com.np', 'Tutorials Point')->subject
            ('Laravel HTML Testing Mail');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "HTML Email Sent. Check your inbox.";
    }

    public function attachment_email(){
        $data = array('name'=>"Virat Gandhi");
        Mail::send('mail', $data, function($message) {
            $message->to('abc@gmail.com', 'Tutorials Point')->subject
            ('Laravel Testing Mail with Attachment');
            $message->attach('C:\laravel-master\laravel\public\uploads\image.png');
            $message->attach('C:\laravel-master\laravel\public\uploads\test.txt');
            $message->from('xyz@gmail.com','Virat Gandhi');
        });
        echo "Email Sent with attachment. Check your inbox.";
    }
}