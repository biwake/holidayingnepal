<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Home;


use App\Models\Category;
use App\Models\Days;
use App\Models\Destination;
use App\Models\Person;
use App\Models\Position;
use App\Models\Post;
use App\Models\Pperson;
use App\Models\Price;
use DB, File;
use Illuminate\Http\Request;

class DestinationController extends HomeBaseController
{
    protected $base_route = 'destination';
    protected $view_path = 'home.destination';

    public function index(Request $request, $slug)
    {

        $data = [];
        //$data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->groupBy('aid')->paginate(15);
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        $data['slug'] = Destination::select('id','name','slug')->where('slug',$slug)->first();
        $data['rows'] = Destination::select('des.pid as pid')
            ->leftjoin('despost as des','des.did', '=', 'destination.id')
            ->where('slug',$slug)
            ->groupBy('des.pid')
            ->paginate(10);
        $data['days'] = Days::select('id','name')->get();
        $data['price'] = Price::select('id','name')->get();
        $data['suggest'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('suggested','y')
            ->groupby('article_cat.aid')
            ->limit(7)
            ->get();
        $data['popular'] = Post::select('name','article_cat.cid as cid','slug')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('popular','!=',0)->OrderBy('popular','desc')->groupBy('article_cat.aid')->limit(7)->get();
        $data['foot'] = Category::select('name','slug')->where('cat',0)->where('type','!=','about')->where('type','!=','contact')->limit(5)->get();


        return view(parent::loadDefaultVars($this->view_path.'.index'), compact('data'));
    }

    public function detail(Request $request, $id, $slug)
    {
        $data = [];
        //$data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->groupBy('aid')->paginate(15);
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        //$data['slug'] = Category::select('id','name','description')->where('slug',$slug)->first()
        $data['row']=Post::select('*')->where('slug',$slug)->first();
        $data['foot'] = Category::select('name','slug')->where('cat',0)->where('type','!=','about')->where('type','!=','contact')->limit(5)->get();
        return view(parent::loadDefaultVars('home.detail.index'), compact('data'));
    }

    public function personprice(Request $request)
    {
        $data = [];
        $package = Post::select('aid')->where('slug',$request->get('package'))->first();
        $person = Person::select('id')->where('name',$request->get('person'))->first();

        $data['value'] = Pperson::select('price','per')->where('aid',$package->aid)->where('pid',$person->id)->first();

        return view('home.destination.include.pperson', compact('data'));
    }

    public function personpriceno(Request $request)
    {
        $person = Person::select('start','end')->where('name',$request->get('person'))->first();
        $value = $request->get('no');
        if ($value<$person->start){
            return 'no must be '.$person->start.' - '.$person->end;
        }
        if ($value>$person->end){
            return 'no must be '.$person->start.' - '.$person->end;
        }
        return $request->get('no') * $request->get('per');
    }





}