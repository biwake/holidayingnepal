<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 11/15/16
 * Time: 7:29 AM
 */
namespace App\Http\Controllers\Home;


use App\Models\Category;
use App\Models\Imagetbl;
use App\Models\Person;
use App\Models\Position;
use App\Models\Post;
use DB, File;
use Illuminate\Http\Request;

class IndexController extends HomeBaseController
{
    protected $base_route = 'home.home';
    protected $view_path = 'home.home';

    public function index(Request $request)
    {
        $data = [];
        //$data['rows'] = Post::select('id','name','tags','slug','aid','status','created_at')->groupBy('aid')->paginate(15);
        $data['menu'] = Category::select('id')->where('cat',0)->where('status','y')->get();
        $data['position'] = Position::select('id','cid','title','pos')->orderBy('pos','asc')->get();
        $data['image'] = Imagetbl::select('image')->where('id',1)->first();
        $data['slider'] = Post::select('name','detail','slug', 'article.aid as aid', 'article_cat.cid as cid')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->where('suggested','y')
            ->groupby('article_cat.cid')
            ->limit(5)->get();
        $data['foot'] = Category::select('name','slug')->where('cat',0)->where('type','!=','about')->where('type','!=','contact')->limit(5)->get();
        $data['people'] = Person::select('name')->get();
        $data['pp'] = 'home';
        return view(parent::loadDefaultVars($this->view_path.'.index'), compact('data'));
    }





}