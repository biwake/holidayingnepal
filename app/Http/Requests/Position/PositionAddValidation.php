<?php

namespace App\Http\Requests\Position;

use Illuminate\Foundation\Http\FormRequest;

class PositionAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|min:5',
            'parent'              =>  'required',
            'position'              =>  'required|unique:position,pos|numeric',
        ];
    }

    public function messages() {
        return [
            'name.required'                => 'Please, Add Name.',
            'name.min'                     => 'Please, Add min 5 characters.',
            'parent.required'                     => 'Please, select at least one category',
            'position.required'                     => 'Please, Add position',
            'position.unique'                     => 'Added position already exits. Please input unique number.',
            'position.number'                     => 'position must be number.',

        ];
    }
}
