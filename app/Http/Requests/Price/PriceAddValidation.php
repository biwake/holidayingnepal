<?php

namespace App\Http\Requests\Price;

use Illuminate\Foundation\Http\FormRequest;

class PriceAddValidation extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'              =>  'required|min:3',
            'start'              =>  'required',
            'end'              =>  'required',
        ];
    }

    public function messages() {
        return [
            'name.required'                => 'Please, Add Name.',
            'name.min'                     => 'Please, Add min 3 characters.',
            'start.required'                => 'Please, Add start price.',
            'end.required'                => 'Please, Add end price.',

        ];
    }
}
