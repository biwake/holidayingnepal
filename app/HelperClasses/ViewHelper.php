<?php
/**
 * Created by PhpStorm.
 * User: dell
 * Date: 11/7/2016
 * Time: 7:52 AM
 */

namespace App\HelperClasses;


use App\Models\Articlecid;
use App\Models\Category;
use App\Models\Despost;
use App\Models\Person;
use App\Models\Post;
use App\Models\Pperson;
use Request;

class ViewHelper
{
    public function getData($key, $data = [])
    {
        if (old($key))
            return old($key);
        elseif (count($data) > 0)
            return $data->$key;
        else
            return '';
    }

    public function getmenu($id)
    {
        if (Category::select('id')->where('cat',$id)->count()==0){
            $typecheck = Category::select('type')->where('id',$id)->first();
            if ($typecheck->type=="about"){
                echo rtrim('<li><a href='.route('about.index').'>'.$this->getmenuname($id).'</a></li>');
            }elseif ($typecheck->type=="contact"){
                echo rtrim('<li><a href='.route('contact.index').'>'.$this->getmenuname($id).'</a></li>');
            }else {
                echo rtrim('<li><a href=' . route('category.index', ['slug' => $this->getmenuslug($id)]) . '>' . $this->getmenuname($id) . '</a></li>');
            }
        }else{
            $data['name']=$this->getmenuname($id);
            $data['slug']=$this->getmenuslug($id);
            $data['id'] = $id;
            echo view('home.common.menu.submenu', ['data'=>json_encode($data)]);
            //echo rtrim('<li><a href='.$this->getmenuslug($id).'>'.$this->getmenuname($id).'</a></li>');
        }
    }

    protected function getmenuname($id){
        $getname = Category::select('name')->where('id',$id)->first();
        return $getname->name;
    }

    protected function getmenuslug($id){
        $getname = Category::select('slug')->where('id',$id)->first();
        return $getname->slug;
    }

    public function getsubmenu($id)
    {
        $data = [];
        $data['value']=Category::select('name','slug')->where('cat',$id)->get();
        echo view('home.common.menu.thirdmenu', ['data'=>json_encode($data)]);
    }

    public function getarticle($cid,$title)
    {

        $data['row'] = Articlecid::select('article.id as id','article.name as name','article.image as image','article.slug as slug','article.price as price', 'days.name as days')
            ->leftjoin('article', 'article.aid', '=', 'article_cat.aid')
            ->leftjoin('days', 'days.id', '=', 'article.days')
            ->where('article_cat.cid',$cid)->where('article.status','y')
            ->where('article.featured','y')
            ->OrderBy('article.id','desc')
            ->limit(5)
            ->groupby('article_cat.aid')
            ->get();
        $data['slug'] = Category::select('id','name','description','slug')->where('id',$cid)->first();
        $data['people'] = Person::select('name')->get();
        $data['title'] = $title;
        echo view('home.home.grid', ['data'=>json_encode($data)]);
    }

    public function getdestination($aid)
    {
        $data = [];
        $data['destination'] = Despost::select('destination.name as name','destination.slug as slug')
            ->leftjoin('destination', 'destination.id', '=', 'despost.did')
            ->where('pid',$aid)->get();

        if (count($data['destination'])==0){
            return '';
        }else {
            echo view('home.inner.destination', ['data' => json_encode($data)]);
        }
    }

    public function getdesgrid($pid,$abc)
    {
        $data=[];

        $data['row'] = Despost::select('despost.pid as pid','article.name as name', 'article_cat.cid as cid', 'article.aid as aid', 'article.status as status', 'article.created_at as created_at', 'article.image as image', 'days.name as days','article.price as price', 'article.slug as slug', 'category.slug as catslug')
            ->leftjoin('article', 'article.aid', '=', 'despost.pid')
            ->leftjoin('article_cat', 'article_cat.aid', '=', 'article.aid')
            ->leftjoin('days', 'days.id', '=', 'article.days')
->leftjoin('category', 'category.id', '=', 'article_cat.cid')
            ->where('article.aid',$pid)
->groupby('article_cat.aid')
            ->get();

        $data['people'] = Person::select('name')->get();
        echo view('home.destination.grid', ['data' => json_encode($data)])->render();
    }

    public function getpersonprice($name)
    {
        $value = Person::select('pperson.price as price')
            ->leftjoin('pperson','pperson.pid', '=', 'person.id')
            ->where('name',$name)
            ->first();

        return rtrim($value->price);
    }

    public function getsliderlink($id,$slug)
    {
        $get = Category::select('slug')->where('id',$id)->first();

        return route('category.detail', ['id'=>$get->slug,'slug'=>$slug]);
    }

    public function getsearchlink($id,$slug)
    {
        $get = Category::select('slug')->where('id',$id)->first();
        return route('category.detail', ['id'=>$get->slug,'slug'=>$slug]);
    }

    public function getpriceindetail($aid,$pid)
    {
        $get = Pperson::select('price')->where('aid',$aid)->where('pid',$pid)->first();
        if (count($get)==0){
            return 'Not Available';
        }else{
            return rtrim('IRs '.$get->price);
        }
    }

    public function letcheck()
    {

        $location = file_get_contents('http://ip-api.com/json/'.Request::ip());
        $pp=json_decode($location);
        return rtrim($pp->country);
        //return 'Nepal';

    }

    public function letmegetit($cid)
    {
$abc = Articlecid::select('cid')->where('aid',$cid)->first();
        $mno = Category::select('slug')->where('id',$abc->cid)->first();
        return rtrim($mno->slug);
    }

}